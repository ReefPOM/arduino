
/*This program loads a new Arduino with temporary EEPROM data.
 *Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
*/ 
#include <EEPROMex.h>  //For Calibration Data and ID

//THIS PROGRAM NEEDS TO BE UPDATED
//   EVERYTHING HAS BEEN RE-ARRANGED
//   SET ALL ELEMENT TYPES AND ELEMENT FUNCTIONS TO '10'

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gar00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"ssm00001", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"ssm00002", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"s0000003", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"s0000004", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"s0000005", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"s0000006", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"s0000007", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"s0000008", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"s0000009", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"s0000010", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"s0000011", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"s0000012", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"s0000013", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"s0000014", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"s0000015", 8);  //Element #15 ID
  EEPROM.updateBlock<char>(294,"s0000016", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"s0000017", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"s0000018", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"s0000019", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"s0000020", 8);  //Element #20 ID


    //write stock Element Types
        //s=sensor, a=actuator, n=nothing, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"s", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"s", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"z", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"z", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"z", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"z", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"z", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"z", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"z", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"z", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"z", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"z", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"z", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"z", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"z", 1);  //Element #15 Type
  EEPROM.updateBlock<char>(302,"z", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"z", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type


    //Write stock Element Functions
       //0=unused, 1=analogRead, 2=digitalRead,
       //=analogWrite, 4=digitalWrite, 10=Loaded by this program
  EEPROM.updateInt(18, 1);  //Elemenr #1 Function
  EEPROM.updateInt(37, 1);  //Elemenr #2 Function
  EEPROM.updateInt(56, 10);  //Elemenr #3 Function
  EEPROM.updateInt(75, 10);  //Elemenr #4 Function
  EEPROM.updateInt(94, 10);  //Elemenr #5 Function
  EEPROM.updateInt(113, 10);  //Elemenr #6 Function
  EEPROM.updateInt(132, 10);  //Elemenr #7 Function
  EEPROM.updateInt(151, 10);  //Elemenr #8 Function
  EEPROM.updateInt(170, 10);  //Elemenr #9 Function
  EEPROM.updateInt(189, 10);  //Elemenr #10 Function
  EEPROM.updateInt(208, 10);  //Elemenr #11 Function
  EEPROM.updateInt(227, 10);  //Elemenr #12 Function
  EEPROM.updateInt(246, 10);  //Elemenr #13 Function
  EEPROM.updateInt(265, 10);  //Elemenr #14 Function
  EEPROM.updateInt(284, 10);  //Elemenr #15 Function
  EEPROM.updateInt(303, 10);  //Elemenr #16 Function
  EEPROM.updateInt(322, 10);  //Elemenr #17 Function
  EEPROM.updateInt(341, 10);  //Elemenr #18 Function
  EEPROM.updateInt(360, 10);  //Elemenr #19 Function
  EEPROM.updateInt(379, 10);  //Elemenr #20 Function


    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 1);  //Element #1 Slope / FS Val
  EEPROM.updateFloat(24, 0);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, 1);  //Element #2 Slope / FS Val
  EEPROM.updateFloat(43, 0);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 1);  //Element #3 Slope / FS Val
  EEPROM.updateFloat(62, 0);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, 1);  //Element #5 Slope / FS Val
  EEPROM.updateFloat(100, 0);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra

  

    //Element Pins
  EEPROM.updateInt(389, 14);  //Element #1 Pin
  EEPROM.updateInt(391, 15);  //Element #2 Pin
  EEPROM.updateInt(393, 99);  //Element #3 Pin
  EEPROM.updateInt(395, 99);  //Element #4 Pin
  EEPROM.updateInt(397, 99);  //Element #5 Pin
  EEPROM.updateInt(399, 99);  //Element #6 Pin
  EEPROM.updateInt(401, 99);  //Element #7 Pin
  EEPROM.updateInt(403, 99);  //Element #8 Pin
  EEPROM.updateInt(405, 99);  //Element #9 Pin
  EEPROM.updateInt(407, 99);  //Element #10 Pin
  EEPROM.updateInt(409, 99);  //Element #11 Pin
  EEPROM.updateInt(411, 99);  //Element #12 Pin
  EEPROM.updateInt(413, 99);  //Element #13 Pin
  EEPROM.updateInt(415, 99);  //Element #14 Pin
  EEPROM.updateInt(417, 99);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 99);  //Element #18 Pin
  EEPROM.updateInt(425, 99);  //Element #19 Pin
  EEPROM.updateInt(427, 99);  //Element #20 Pin  //99 signifies no pin
}

void loop() {
  // put your main code here, to run repeatedly:

}

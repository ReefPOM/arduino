/*  OSPOM PiBox MainBoard v1 Arduino program 
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
--------------
2  Relay #1 Signal
3  PWM #1
4  Relay #2 SIgnal
5  PWM #2
6  PWM #3
7  Servo #1 Signal
8  Servo #2 Signal
9  Red LED
10 Green LED
11 Blue LED
12 While LED
13
14 Motion Sensor
15 
16 3.3v input reference
17 12v input for monitoring
18 SDA (for temp/humid)
19 SCL (for temp/humid)
20 Ambient Light
21 Smoke Detector Input

PiBox MainBoard Element List:
Element, Function, Pin#
------------------------
1     , Relay#1   , 2
2     , Relay#2   , 4
3     , Servo#1   , 7
4     , Servo#2   , 8
5     , Motion    , 14
6     , Smoke     , 21
7     , AmbLight  , 20 
8     , WhiteLED  , 12
9     , R led     , 9
10    , G led     , 10
11    , B led     , 11
12    , 3.3v in   , 16
13    , 12v in    , 17
14    , Humidity  , I2C
15    , Temp      , I2C
16    , pwm#1     , 3
17    , pwm#2     , 5
18    , pwm#3     , 6
*/


#include <Servo.h>
#include <HTU21D.h>
#include <Wire.h>

HTU21D myHumidity;  //Create an instance of the object
Servo servo1;  // create servo object to control a servo
Servo servo2;  // create servo object to control a servo


unsigned long previousMillis = 0;
const char GroupID[9] = 'gmb00000'; //*when declaring a char array, you add 1 more than the number of chars, 



void setup()
{
  Serial.begin(57600);
  InputString.reserve(200);  // reserve 200 bytes for the inputString:
  myHumidity.begin();
  servo1.attach(7);  // attaches the servo on pin 7 to the servo object
  servo2.attach(8);  // attaches the servo on pin 8 to the servo object

      //Ask odroid for Actuator Data at Startup
  Serial.print(GroupID);
  Serial.print("/");
  Serial.println("SendActData");
  startupAid = true;

  
}



void loop() {
  if (incomingUSBmsg()) {
    //handle the msg
  }




  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= 1000) {
    previousMillis = currentMillis;     // save the last time
    float humd = myHumidity.readHumidity();
    float temp = myHumidity.readTemperature();
    temp = temp * 1.8;
    temp = temp + 32;
    Serial.println(humid);
    Serial.println(temp);
    //Put something to control the servo's in here

  }





  
}

/************************************************************************************************/
//Deals with incoming Serial Messages
void incomingUSBmsg(void) {
 while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    InputString += inChar;
    // if the incoming character is a '!', see who it's for
    if (inChar == '!') {
      String GroupIDIn = "";
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char inChar = InputString.charAt(i);
        GroupIDIn += inChar;
      }
      if (GroupIDIn == GroupID) {   //If the ID from serial matches the ID from eeprom
      if (SendData) {
      Pause = true;  //if it's sending continuous data, stop for a bit
      }
        //decide what kind of message it is
  if ((InputString.charAt(8) == 's') || (InputString.charAt(8) == 'a')) {
          ElementCommand = true;
    stringcomplete = true;
          elementParse();
  }
        else if (((InputString.charAt(8) - '0') >= 0) && ((InputString.charAt(8) - '0') <=9)) {
          groupCommand = true;
    stringcomplete = true;
        }
        else {
          fail();
    }
      }
      else if ((InputString.charAt(0) == '0') && (InputString.length() == 2)) {
        sendGroupID();
     }
     else {
       fail();  //Return a -1
     }
     GroupIDIn = "";
    }

  }
  return stringcomplete;
}

/************************************************************************************************/
void OspomFeather::elementParse(void) {
/* 
 
 char ElementIDIn[9] = "        ";
 String serialReadString = "";
      //Load the Element ID
  for (int i = 8; i < 16; i++) {  
    char InChar = InputString.charAt(i);
    serialReadString += InChar;
  }
      //convert to Char array for comparison
  serialReadString.toCharArray(ElementIDIn, (serialReadString.length()+1));  //+1 to make it work
  serialReadString = ""; 
      //Compare it to the string of stored element id's, and say which # it is
  boolean foundElement = true;
  for (int i = 0; i < 15; i++) {
      char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
      
      if (strcmp(ElementID, ElementIDIn) == 0) {  //strcmp compares the two char arrays & returns a 0 if they are the same
        ElementNumber = i;
        foundElement = false;
      }
      if ((i == (15 - 1)) && (foundElement)) {
        fail();
      }
  }
  //clear the string
  serialReadString = "";
     //Load the Element Command Character  
  ElementCommandChar = InputString.charAt(16);
     //Load the Element Command Value
  for (int i = 18; i < 26; i++) {  
    char InChar = InputString.charAt(i);
    serialReadString += InChar;
  }
  serialReadString.toCharArray(CommandCharArray, (serialReadString.length()+1));   //+1 to make it work
  ElementCommandFloat = serialReadString.toFloat();
  ElementCommandInt = serialReadString.toInt();
  serialReadString = ""; 

 */
}


/************************************************************************************************/
void success(void)
{
  Serial.print(GroupID);  //Print out ID
  Serial.print("/");
  Serial.println("1");
  clearTheMsg();
}

/************************************************************************************************/
void fail(void)
{
  Serial.print(GroupID);  //Print out ID
  Serial.print("/");
  Serial.println("-1");
  clearTheMsg();
}

/************************************************************************************************/
void clearTheMsg(void)
{
  InputString = "";
  stringcomplete = false;
  groupCommand = false;
  ElementCommand = false;
  for(int i = 0; i < 8; i++) {
    CommandCharArray[i] = ' ';
  }   
  ElementCommandChar = ' ';
  ElementNumber = 0;
  ElementCommandFloat = 0;
  ElementCommandInt = 0;
}



















//This is the main function that gets called from the Arduino program
void OspomRun(void)
{
const char Element1ID[9] = 'arl00001';
const char Element2ID[9] = 'arl00002';
const char Element3ID[9] = 'asv00001';
const char Element4ID[9] = 'asv00002';
const char Element5ID[9] = 'smot0000';
const char Element6ID[9] = 'ssmk0000';
const char Element7ID[9] = 'sal00000';
const char Element8ID[9] = 'aled0001';

  
  //Having these written out twice as const int's saves a lot of RAM
  const int  EEPIDLoc[16] = {9,28,47,66,85,104,123,142,161,180,199,218,237,256,275};
  //Location of Element Type (Sensor, actuator)
  const int EEPTypLoc[16] = {17,36,55,74,93,112,131,150,169,188,207,226,245,264,283};
  //Location of Element Function (Inactive, Analog Read, Digital Read, Analog Write, ect)
  const int EEPFuncLoc[16] = {18,37,56,75,94,113,132,151,170,189,208,227,246,265,284};
  //Location of Element Slope(if sensor) or Fail Safe(if actuator) value
  const int EEPSlopeFSLoc[16] = {20,39,58,77,96,115,134,153,172,191,210,229,248,267,286};
  //Location of Element Y-intercept(if sensor) or Extra(if actuator)
  const int EEPYintExLoc[16] = {24,43,62,81,100,119,138,157,176,195,214,233,252,271,290};
  const int EEPCLoc[16] = {473,477,481,485,489,493,497,501,505,509,513,517,521,525,529};

  unsigned long TimeNow = 0;
  static unsigned long SensorStreamStartTime;
  static unsigned long LastMeasurement;
  static unsigned long LastSend;
  static unsigned int SendDelay;

  if (incomingUSBmsg()) {
//    Serial.print("ElementCommandChar: ");
//    Serial.println(ElementCommandChar);
//    Serial.print("ElementCommandFloat: ");   
//    Serial.println(ElementCommandFloat);


    if (groupCommand) {
      //Do Group Command Stuff
      switch (groupCommandVal()) {
        case 0:
          sendGroupID();
          break;
        case 10:
          sendSensorData();
          break;
        case 11:
    SendDelay = GroupCommandInt;  //Grab the time in milliseconds
          SensorStreamStartTime = millis();                   //to delay before sending again
          SendData = true;
          break;
        case 12:
          sendAllSensorIDs();
          break;
        case 13:
          sendActuatorData();
          break;
        case 14:
          sendAllActuatorIDs();
          break;
        case 17:
          watchDog();
          break;
        case 18:
          setGroupID();
          break;
        default:
          fail();
          break;
      }
    }
  //Element Command Section accepts commands and takes action
    else if (ElementCommand) {
  //Store the Element ID we are working with in a temporary variable
      char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
      EEPROM.readBlock<char>(EEPIDLoc[ElementNumber], ElementID, 8);  //Reads the Element ID from EEPROM
      //Do what the command Character says
      if (ElementCommandChar == 'R') {
         //Read the element value, and send it back to the odroid 
        Serial.print(GroupID);
        Serial.print("/");
        Serial.print(ElementID);
        Serial.print(":");
        Serial.println(ElementCalVal[ElementNumber]);
      }
      else if (ElementCommandChar == 'I') {
         //Change the Elements ID
        EEPROM.updateBlock<char>(EEPIDLoc[ElementNumber], CommandCharArray, 8);
        success();
      }
      else if (ElementCommandChar == 'T') {
         //Change the Elements Type (s = sensor, a = actuator, z=SetBy EEPROM Loader Program)
  EEPROM.write(EEPTypLoc[ElementNumber],CommandCharArray[0]);
  ElementType[ElementNumber] = EEPROM.read(EEPTypLoc[ElementNumber]);
        success();
      }
      else if (ElementCommandChar == 'C') {
         //Change the Elements Function (0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 
      // 4=digitalWrite, 5=Triac, 10=SetBy EEPROM Loader Program)
  EEPROM.updateInt(EEPFuncLoc[ElementNumber], ElementCommandInt);
  ElementFunction[ElementNumber] = EEPROM.readInt(EEPFuncLoc[ElementNumber]);
        success();
      }
      else if (ElementCommandChar == 'A') {
  //Read the Element Type and Send it back to the odroid
  Serial.print(GroupID);
        Serial.print("/");
        Serial.print(ElementID);
        Serial.print(":");
        Serial.println(ElementType[ElementNumber]);
      }
      else if (ElementCommandChar == 'D') {
  //Read the Element Function and Send it back to the odroid
  Serial.print(GroupID);
        Serial.print("/");
        Serial.print(ElementID);
        Serial.print(":");
        Serial.println(ElementFunction[ElementNumber]);
      }
      else if (ElementCommandChar == 'M') {
        //Change the Elements Calibration Slope
        EEPROM.updateFloat(EEPSlopeFSLoc[ElementNumber], ElementCommandFloat);  //Save value to EEPROM);
        ElementSlopeFS[ElementNumber] = ElementCommandFloat;
        success();
      }
      else if (ElementCommandChar == 'B') {
        //Change the Elements Calibration Slope
        EEPROM.updateFloat(EEPYintExLoc[ElementNumber], ElementCommandFloat);  //Save value to EEPROM);
        ElementYintEx[ElementNumber] = ElementCommandFloat;
        success();
      }
      else if (ElementCommandChar == 'Q') {
        //Change the Elements Calibration C
        EEPROM.updateFloat(EEPCLoc[ElementNumber], ElementCommandFloat);  //Save value to EEPROM);
        ElementC[ElementNumber] = ElementCommandFloat;
        success();
      }
      else if (ElementCommandChar == 'W') {
  //Write (Set) the Actuator
  if (ElementType[ElementNumber] == 'a') {  //Make sure it's an actuator (so we don't break sensors)
    if ((ElementFunction[ElementNumber] == 3) && (ElementCommandInt != 100) && (ElementCommandInt != 0)) {  //AnalogWrite
      if ((ElementCommandInt > 0) && (ElementCommandInt < 100)) {
        ElementCalVal[ElementNumber] = ElementCommandInt;  //Save the Actuator Value so it can tell what it is if asked
        ElementCommandInt = ElementCommandInt * 255 / 100; //convert to 0-255 for PWM
        analogWrite(ElementPin[ElementNumber], ElementCommandInt);  //AnalogWrite (PWM) if it's a 1-99 value
        success();
      }
      else {
        fail();
      }
    }     //This does the digital writes, and also takes care of 100% on and 100% off PWM dimming
    else if ((ElementFunction[ElementNumber] == 4) || (ElementFunction[ElementNumber] == 3)) {  //DigitalWrite
      ElementCalVal[ElementNumber] = ElementCommandInt;  //Save the Actuator Value so it can tell what it is if asked
      if (ElementCommandInt == 0) { //0 means turn off the actuator
        digitalWrite(ElementPin[ElementNumber], LOW);
        success();
      }
      else if (ElementCommandInt == 100) {  //100 means turn on the actuator
        digitalWrite(ElementPin[ElementNumber], HIGH);
        success();
      }
      else {
        fail();
      }
    }
    else {
      fail();
    }
    }
  else {
    fail();
  }
      }
      else if (ElementCommandChar == 'S') {  //Not sure if storing an in where a float goes sometimes will matter
  //Set Fail Safe Value
  EEPROM.updateInt(EEPSlopeFSLoc[ElementNumber], ElementCommandInt);
  ElementSlopeFS[ElementNumber] = ElementCommandInt;
        success();
      }
      else if (ElementCommandChar == 'F') {
  //Read Fail Safe Value
  Serial.print(GroupID);
        Serial.print("/");
        Serial.print(ElementID);
        Serial.print(":");
        Serial.println(EEPROM.readInt(EEPSlopeFSLoc[ElementNumber]));
      }
      else {
        fail();  //Return a -1
      }
      clearTheMsg(); 

    }
    else {
      fail();
    }

    clearTheMsg();
  }

  //Measure sensor elements (with the delay decided in the main sketch), put them in an array
  TimeNow = millis();  //Check the time
  if (TimeNow > LastMeasurement + 1000) {
    LastMeasurement = millis();
    readSensors();  
  }

  //Send a Stream of Sensor Data for 1 minute if it has been requested
  if (SendData) {
    if (SensorStreamStartTime + 60000 > TimeNow)  {  //Send Data Stream
      if (Pause) {
        Pause = false;
      }
      else {
        if (TimeNow > LastSend + SendDelay) {
          sendSensorData();
          LastSend = millis();
        }
      }
    }
    else {  //  Stop Sending Data
      SendData = false;
      SensorStreamStartTime = 0;
      SendDelay = 0;
      LastSend = 0;
    }
  }
}
















/************************************************************************************************/
int OspomFeather::groupCommandVal(void) {
       //Secondary Serial String Processing Input to use in group commands
  int CommandInt = InputString.charAt(8) - '0';  //converting char to int
  int Comand1 = InputString.charAt(9) - '0';  //converting char 2 to int
  if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
    CommandInt = CommandInt * 10;
    CommandInt += Comand1;  
  } 
//This part gets further command info
  String serialReadString = "";
  if (((InputString.charAt(9) - '0') >= 0) && ((InputString.charAt(9) - '0') <=9)) {  //If it is a 2 digit request
    for (int i = 11; i < InputString.length(); i++) {
      char ActuatorRead = InputString.charAt(i);
      serialReadString += ActuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i < InputString.length(); i++) {
      char ActuatorRead = InputString.charAt(i);
      serialReadString += ActuatorRead;
    }
  }
  serialReadString.toCharArray(CommandCharArray, serialReadString.length());    //convert to Char array
  GroupCommandInt = serialReadString.toInt();  //An integer of the serial input
  return CommandInt;
}

/************************************************************************************************/
void OspomFeather::sendGroupID(void) {
  Serial.print(GroupID);
  Serial.print("/");
  Serial.println(GroupID);
  clearTheMsg();
}

/************************************************************************************************/
void OspomFeather::sendSensorData(void) {
//Send Back Calibrated Sensor Data to Ai
    //ToDo: Put this array in PROGMEM
  const int  EEPIDLoc[16] = {9,28,47,66,85,104,123,142,161,180,199,218,237,256,275};
  Serial.print(GroupID);  //Group ID
  Serial.print("/");
    //Count How many Sensors we have to send data for
  int NumberOfSensors = 0;
  for (int i = 0; i < 15; i++) {   
    if ((ElementType[i] == 's') && (ElementFunction[i] != 0)) {
      NumberOfSensors++;
    }
  }
    //Send The Data
  int NumberOfSentSensors = 0;
  if (NumberOfSensors == 0) {  //Send a 0 if there are no active sensors
    Serial.println("0");
  }
  else {   //Send The Data
    for (int i = 0; i < 15; i++) {   
      if ((ElementType[i] == 's') && (ElementFunction[i] != 0)) {   //Type: s = sensor, a = actuator
       //Function: 0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 4=digitalWrite, 5=triac, 10=EEPLoaded
        NumberOfSentSensors++;
        char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
        EEPROM.readBlock<char>(EEPIDLoc[i], ElementID, 8);  //Reads the Element ID from EEPROM
        Serial.print(ElementID);
        Serial.print(":");
        Serial.print(ElementCalVal[i]);
        if (NumberOfSentSensors < NumberOfSensors) {  //This makes it so there isnt an extra comma at the end.
    Serial.print(",");
        }
        else {
    Serial.println();
        }
      }
    }
  }
  clearTheMsg();
}

/************************************************************************************************/
void OspomFeather::sendActuatorData(void) {
   //ElementCalVal when speaking of an Actuator refers to it's value
    //ToDo: Put this array in PROGMEM
  const int  EEPIDLoc[16] = {9,28,47,66,85,104,123,142,161,180,199,218,237,256,275};
  Serial.print(GroupID);  //Group ID
  Serial.print("/");
    //Count How many Actuators we have to send data for
  int NumberOfActuators = 0;
  for (int i = 0; i < 15; i++) {   
    if ((ElementType[i] == 'a') && (ElementFunction[i] != 0)) {
      NumberOfActuators++;
    }
  }
   
  int NumberOfSentActuators = 0;
  if (NumberOfActuators == 0) {  //Send a 0 if there are no active actuators
    Serial.println("0");
  }
  else {   //Send The Data
    for (int i = 0; i < 15; i++) {   
      if ((ElementType[i] == 'a') && (ElementFunction[i] != 0)) {   //Type: s = sensor, a = actuator
       //Function: 0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 4=digitalWrite, 5=triac, 10=EEPLoaded
        char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
        NumberOfSentActuators++;
        EEPROM.readBlock<char>(EEPIDLoc[i], ElementID, 8);  //Reads the Element ID from EEPROM
        Serial.print(ElementID);
        Serial.print(":");
        Serial.print(ElementCalVal[i]);
        if (NumberOfSentActuators < NumberOfActuators) {  //This makes it so there isnt an extra comma at the end.
    Serial.print(",");
        }
        else {
    Serial.println();
        }
      }
    }
  }
  clearTheMsg();
}


/************************************************************************************************/
void OspomFeather::setGroupID(void) {
  EEPROM.updateBlock<char>(0,CommandCharArray, 8);  //Write ID
  EEPROM.readBlock<char>(0, GroupID, 8);  //Read ID
  success();
}





/************************************************************************************************/
void OspomFeather::readSensors(void) {
  static int index;
  numReadings = 5;
  boolean advance = true;

  for (int i = 0; i < 15; i++) {
    if (ElementType[i] == 's') {   //s = sensor, a = actuator
      if (ElementFunction[i] == 1) {  // 0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 4=digitalWrite, 5=triac
            //6=Flow, 7=Level, 10=Loaded by the generic EEPROMLoader program
        //Analog Reading Section
        ElementVal[i][index] = ElementVal[i][index] + analogRead(ElementPin[i]); // add 3 sensor readigns together
        ElementTotalValue[i] = ElementTotalValue[i] + ElementVal[i][index]; // add the reading to the total
            // calculate the average: 
        if (startupAid){
          ElementAvg[i] = ElementTotalValue[i] / (index + 1);   //fixes wrong avg at the beginning
        }
        else {
          ElementAvg[i] = (ElementTotalValue[i] / numReadings);
        }
            // 2 point calibration
        if (ElementC[i] == 0) { //straight line calibration    
          ElementCalVal[i] = ElementAvg[i] - ElementYintEx[i];
          ElementCalVal[i] = ElementCalVal[i] / ElementSlopeFS[i];
        }
       else if (ElementC[i] == 1)   {   // 3 point calibration
//           Serial.print("3point: ");
//           Serial.println(i);
           ElementCalVal[i] = (-ElementYintEx[i] + sqrt((ElementYintEx[i]*ElementYintEx[i])-(4*ElementSlopeFS[i]*(-ElementAvg[i])))) / (2*ElementSlopeFS[i]);
          // float x = (-b + sqrt((b*b)-(4*a*(-y)))) / (2*a);
          // ElementCalVal[i] = (-ElementYintEx[i] + sqrt((ElementYintEx[i]*ElementYintEx[i])-(4*ElementSlopeFS[i]*(-ElementAvg[i])))) / (2*ElementSlopeFS[i]);
        }
          
      }
      else if (ElementFunction[i] == 2) {
  //Digital Reading Section         THis could be fixed up a little.  It helps smooth, but isent perfect
        ElementVal[i][index] = digitalRead(ElementPin[i]);
        int lastindex = index - 1;  //we use this so two digi reads have to the the same to call it true
        if (index == 0) {         //   this helps reduce flicker
          lastindex = 5;
        }
        if ((ElementVal[i][index] == 1) && (ElementVal[i][(lastindex)] == 1)) {
          ElementCalVal[i] = 1;
        }
        else if ((ElementVal[i][index] == 0) && (ElementVal[i][lastindex] == 0)) {
          ElementCalVal[i] = 0;
        }
        else {
        ElementCalVal[i] = ElementCalVal[i];
        }
      }
    }   
  }
  index++;
  if (index >= numReadings) {
    index = 0;
    startupAid = false;
  }
    //Clean Up
  for (int i = 0; i < 15; i++) {
    if (ElementType[i] == 's') {
      if (ElementFunction[i] == 1) {
        ElementTotalValue[i] = ElementTotalValue[i] - ElementVal[i][index]; // subtract the last reading:
        ElementVal[i][index] = 0;  //clear the added up element value
      }
    }
  }
}


/************************************************************************************************/
void OspomFeather::sendAllSensorIDs(void) {
  const int  EEPIDLoc[16] = {9,28,47,66,85,104,123,142,161,180,199,218,237,256,275};
  Serial.print(GroupID);  //Group ID
  Serial.print("/");
    //Count How many Sensors we have to send ID's for
  int NumberOfSensors = 0;
  for (int i = 0; i < 15; i++) {   
    if (ElementType[i] == 's') {
      NumberOfSensors++;
    }
  }
  int NumberOfSentSensors = 0;
  if (NumberOfSensors == 0) {  //Send a 0 if there are no active sensors
    Serial.println("0");
  }
  else {   //Send The ID's
    for (int i = 0; i < 15; i++) {   
      if (ElementType[i] == 's') {   //Type: s = sensor, a = actuator
       //Function: 0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 4=digitalWrite, 5=triac, 10=EEPLoaded
        NumberOfSentSensors++;
        char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
        EEPROM.readBlock<char>(EEPIDLoc[i], ElementID, 8);  //Reads the Element ID from EEPROM
        Serial.print(ElementID);
        Serial.print(":");
        if (ElementFunction[i] == 0) {
          Serial.print("0");
        }
        else {
          Serial.print(ElementCalVal[i]);
        }
        if (NumberOfSentSensors < NumberOfSensors) {  //This makes it so there isnt an extra comma at the end.
          Serial.print(",");
        }
        else {
          Serial.println();
        }
      }
    }
  }
  clearTheMsg();
}

/************************************************************************************************/
void OspomFeather::sendAllActuatorIDs(void) {
  const int  EEPIDLoc[16] = {9,28,47,66,85,104,123,142,161,180,199,218,237,256,275};
  Serial.print(GroupID);  //Group ID
  Serial.print("/");
    //Count How many Actuators we have to send ID's for
  int NumberOfActuators = 0;
  for (int i = 0; i < 15; i++) {   
    if (ElementType[i] == 'a') {
      NumberOfActuators++;
    }
  }
   
  int NumberOfSentActuators = 0;
  if (NumberOfActuators == 0) {  //Send a 0 if there are no active actuators
    Serial.println("0");
  }
  else {   //Send The ID's
    for (int i = 0; i < 15; i++) {   
      if (ElementType[i] == 'a') {   //Type: s = sensor, a = actuator
       //Function: 0=Inactive, 1=analogRead, 2=digitalRead, 3=analogWrite, 4=digitalWrite, 5=triac, 10=EEPLoaded
        char ElementID[9] = "        ";  //A temporary Variable for the ELement ID
        NumberOfSentActuators++;
        EEPROM.readBlock<char>(EEPIDLoc[i], ElementID, 8);  //Reads the Element ID from EEPROM
        Serial.print(ElementID);
        Serial.print(":");
        if (ElementFunction[i] == 0) {
          Serial.print("0");
        }
        else {
          Serial.print(ElementCalVal[i]);
        }
        if (NumberOfSentActuators < NumberOfActuators) {  //This makes it so there isnt an extra comma at the end.
    Serial.print(",");
        }
        else {
    Serial.println();
        }
      }
    }
  }
  clearTheMsg();
}



















/* OSPOM PiBox MainBoard v1 Arduino EEPROM loading program
 * This program loads the PiBox MainBoard with necessary EEPROM data.
 * Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
--------------
2  Relay #1 Signal
3  PWM #1
4  Relay #2 SIgnal
5  PWM #2
6  PWM #3
7  Servo #1 Signal
8  Servo #2 Signal
9  Red LED
10 Green LED
11 Blue LED
12 While LED
13
14 Motion Sensor
15 
16 3.3v input reference
17 12v input for monitoring
18 SDA (for temp/humid)
19 SCL (for temp/humid)
20 Ambient Light
21 Smoke Detector Input

PiBox MainBoard Element List:
Element, Function, Pin#
------------------------
1     , Relay#1   , 2
2     , Relay#2   , 4
3     , Servo#1   , 7
4     , Servo#2   , 8
5     , Motion    , 14
6     , Smoke     , 21
7     , AmbLight  , 20 
8     , WhiteLED  , 12
9     , R led     , 9
10    , G led     , 10
11    , B led     , 11
12    , 3.3v in   , 16
13    , 12v in    , 17
14    , Humidity  , I2C
15    , Temp      , I2C


//Not used due to lack of elements
16    , pwm#1     , 3
17    , pwm#2     , 5
18    , pwm#3     , 6
*/

#include <EEPROMex.h>  //For Calibration Data and ID

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gmb00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"arl00001", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"arl00002", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"asv00001", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"asv00002", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"smot0000", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"ssmk0000", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"sal00000", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"aled0000", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"aled0001", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"aled0002", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"aled0003", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"svo00001", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"svo00002", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"shu00000", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"stfa0000", 8);  //Element #15 ID  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(294,"zzzzzzzz", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"zzzzzzzz", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"zzzzzzzz", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"zzzzzzzz", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"zzzzzzzz", 8);  //Element #20 ID


    //write stock Element Types
        //s=sensor, a=actuator, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"a", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"a", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"a", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"a", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"s", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"s", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"s", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"a", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"a", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"a", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"a", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"s", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"s", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"s", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"s", 1);  //Element #15 Type  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(302,"z", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"z", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type


    //Write stock Element Functions
       //0=inactive, 1=analogRead, 2=digitalRead,
       //3=analogWrite, 4=digitalWrite, 5=triac, 7=level, 8=custom sensor
       //9=custom actuator, 10=Empty, 11=servo
  EEPROM.updateInt(18, 4);  //Element #1 Function
  EEPROM.updateInt(37, 4);  //Element #2 Function
  EEPROM.updateInt(56, 11);  //Element #3 Function
  EEPROM.updateInt(75, 11);  //Element #4 Function
  EEPROM.updateInt(94, 2);  //Element #5 Function
  EEPROM.updateInt(113, 2);  //Element #6 Function
  EEPROM.updateInt(132, 1);  //Element #7 Function
  EEPROM.updateInt(151, 3);  //Element #8 Function
  EEPROM.updateInt(170, 4);  //Element #9 Function (switch to analogWrite when pin is fixed)
  EEPROM.updateInt(189, 4);  //Element #10 Function (Switch to analogWrite when pin is fixed)
  EEPROM.updateInt(208, 3);  //Element #11 Function
  EEPROM.updateInt(227, 1);  //Element #12 Function
  EEPROM.updateInt(246, 1);  //Element #13 Function
  EEPROM.updateInt(265, 8);  //Element #14 Function
  EEPROM.updateInt(284, 8);  //Element #15 Function
  EEPROM.updateInt(303, 10);  //Element #16 Function
  EEPROM.updateInt(322, 10);  //Element #17 Function
  EEPROM.updateInt(341, 10);  //Element #18 Function
  EEPROM.updateInt(360, 10);  //Element #19 Function
  EEPROM.updateInt(379, 10);  //Element #20 Function

    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 1);  //Element #1 Slope / FS Val
  EEPROM.updateFloat(24, 0);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, 1);  //Element #2 Slope / FS Val
  EEPROM.updateFloat(43, 0);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 1);  //Element #3 Slope / FS Val
  EEPROM.updateFloat(62, 0);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, 1);  //Element #5 Slope / FS Val
  EEPROM.updateFloat(100, 0);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val        // y=4.4138x+179.3788
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra


  //Calibration with straight line or parabolic line (0=straight, 1=parabolic)
  EEPROM.updateFloat(473, 0);  //Element #1 C Value
  EEPROM.updateFloat(477, 0);  //Element #2 C Value
  EEPROM.updateFloat(481, 0);  //Element #3 C Value
  EEPROM.updateFloat(485, 0);  //Element #4 C Value
  EEPROM.updateFloat(489, 0);  //Element #5 C Value
  EEPROM.updateFloat(493, 0);  //Element #6 C Value
  EEPROM.updateFloat(497, 0);  //Element #7 C Value
  EEPROM.updateFloat(501, 0);  //Element #8 C Value
  EEPROM.updateFloat(505, 0);  //Element #9 C Value
  EEPROM.updateFloat(509, 0);  //Element #10 C Value
  EEPROM.updateFloat(513, 0);  //Element #11 C Value
  EEPROM.updateFloat(517, 0);  //Element #12 C Value
  EEPROM.updateFloat(521, 0);  //Element #13 C Value
  EEPROM.updateFloat(525, 0);  //Element #14 C Value
  EEPROM.updateFloat(529, 0);  //Element #15 C Value
  EEPROM.updateFloat(533, 0);  //Element #16 C Value

  
  //Element Pins
  EEPROM.updateInt(389, 2);  //Element #1 Pin
  EEPROM.updateInt(391, 4);  //Element #2 Pin
  EEPROM.updateInt(393, 7);  //Element #3 Pin
  EEPROM.updateInt(395, 8);  //Element #4 Pin
  EEPROM.updateInt(397, 14);  //Element #5 Pin
  EEPROM.updateInt(399, 21);  //Element #6 Pin
  EEPROM.updateInt(401, 20);  //Element #7 Pin
  EEPROM.updateInt(403, 12);  //Element #8 Pin
  EEPROM.updateInt(405, 9);  //Element #9 Pin
  EEPROM.updateInt(407, 10);  //Element #10 Pin
  EEPROM.updateInt(409, 11);  //Element #11 Pin
  EEPROM.updateInt(411, 16);  //Element #12 Pin
  EEPROM.updateInt(413, 17);  //Element #13 Pin
  EEPROM.updateInt(415, 99);  //Element #14 Pin
  EEPROM.updateInt(417, 99);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 99);  //Element #18 Pin
  EEPROM.updateInt(425, 99);  //Element #19 Pin
  EEPROM.updateInt(427, 99);  //Element #20 Pin
}

void loop() {
  // put your main code here, to run repeatedly:

}

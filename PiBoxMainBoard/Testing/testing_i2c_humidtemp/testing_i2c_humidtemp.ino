#include <HTU21D.h>

#include <Wire.h>

//serialEvent()
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

//Create an instance of the object
HTU21D myHumidity;

void setup()

{
  Serial.begin(9600);
  inputString.reserve(200);
  Serial.println("HTU21D Example!");
  myHumidity.begin();
}



void loop()

{
  if (inputString == "getall") {
    float humd = myHumidity.readHumidity();
    float temp = myHumidity.readTemperature();
    Serial.print(temp, 1);
    Serial.print(",");
    Serial.println(humd, 1);
    inputString = "";
  }
  delay(10);
}



/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // if the incoming character is a "!", set a flag
    // so the main loop can do something about it:
    if (inChar == '!') {
      stringComplete = true;
    }
    else {
      inputString += inChar;
    }
  }
}

/*  OSPOM PiBox MainBoard v1 Arduino program 
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
--------------
2  Relay #1 Signal
3  PWM #1
4  Relay #2 SIgnal
5  PWM #2
6  PWM #3
7  Servo #1 Signal
8  Servo #2 Signal
9  Red LED
10 Green LED
11 Blue LED
12 While LED
13
14 Motion Sensor
15 
16 3.3v input reference
17 12v input for monitoring
18 SDA (for temp/humid)
19 SCL (for temp/humid)
20 Ambient Light
21 Smoke Detector Input

PiBox MainBoard Element List:
Element, Function, Pin#
------------------------
1     , Relay#1   , 2
2     , Relay#2   , 4
3     , Servo#1   , 7
4     , Servo#2   , 8
5     , Motion    , 14
6     , Smoke     , 21
7     , AmbLight  , 20 
8     , WhiteLED  , 12
9     , R led     , 9
10    , G led     , 10
11    , B led     , 11
12    , 3.3v in   , 16
13    , 12v in    , 17
14    , Humidity  , I2C
15    , Temp      , I2C


//Not used due to lack of elements
16    , pwm#1     , 3
17    , pwm#2     , 5
18    , pwm#3     , 6
*/


#include <Servo.h>
#include <HTU21D.h>
#include <Wire.h>
#include <OspomLite.h>  //include the OspomLite library
#include <EEPROMex.h>  //This must be included for OspomLite to work

HTU21D myHumidity;  //Create an instance of the object
OspomLite ospomLite;  // instantiate ospomLite, an instance of ospomLite Library  Don't put () if it isn't getting a variable
// Servo servo1;  // create servo object to control a servo
// Servo servo2;  // create servo object to control a servo


unsigned long previousMillis = 0;

void setup()

{
  ospomLite.Setup();  //ospomLite.Setup initilizes ospomLite 
  myHumidity.begin();
}



void loop() {
  ospomLite.Run(1000);  //Activates the ospomLite library every time the loop runs **This is required
        //Delay in milliseconds between reads (333 = Read Sensors 3 times a second, 1000 = 1 time a second.)
    /*
    Put any standard arduino code in here that you want to run over and over again.
    ospomLite Functions:
    ospomLite.define(Pin Number, Pin Type, PinFunction, PinID);  = Define ospomLite Internet Dashboard accessable Sensor or Actuator Elements
               example: ospomLite.define(7, 's', ospomLite.anaRead, "stfw0000");
    ospomLite.read();  = Read ospomLite Sensors or Actuators and recieve the result
    ospomLite.write();  = Write ospomLite Sensors or Actuators using this function
       **Writing a pin that is part of the ospomLite dashboard without using this function will cause
           the dashboard to be confused
    ospomLite.Set(element Number(int)), value(float));  = Send your sensor reading to an ospomLite element so it is displayed on the dashboard
    
    **Dont forget to Write your own ospomLite Configuation Sketch to setup your arduino, 
       or run the ospomLite Generic Configuration sketch at least once before calling any ospomLite functions
    */

unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= 1000) {
    previousMillis = currentMillis;     // save the last time
    float humd = myHumidity.readHumidity();
    float temp = myHumidity.readTemperature();
    temp = temp * 1.8;
    temp = temp + 32;
    ospomLite.Set(15, temp);
    ospomLite.Set(14, humd);
  }
}

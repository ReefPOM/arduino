![ReefPom.png](https://bitbucket.org/repo/yz4B4k/images/1187428619-ReefPom.png)

All code is Copyright: GNU General Public License V3.  All Schematics and PCB Files are Licensed: Creative Commons Attribution-ShareAlike 3.0 Unported License

**This means that you can use any of the code, schematics, or pcb's posted in this repository any way you like, however your versions must be licensed in the same way, and open/available to public view/use.*

![ElementOrientedProgramming.png](https://bitbucket.org/repo/yz4B4k/images/663084570-ElementOrientedProgramming.png)
                        *  Check out the [Wiki](https://bitbucket.org/ReefPOM/arduino/wiki/Home) for more details
### Arduino Code for the Reef POM Peripherals ###
![ArduinoSoftwareLayout.png](https://bitbucket.org/repo/yz4B4k/images/3188628876-ArduinoSoftwareLayout.png)

* This Git Repo contains the code which is running on each of the sensors and peripherals of the Reef POM
* All Arduino powered periferals communicate with a group of Python programs running on a Linux computer, aka 'the CPU'.  Download the necessary software from us for free.  Or purchase a Rasberry-Pi, Odroid, or pcDuino from OSPOM.com with the software installed. micro-SD cards containg all the necessary code and operating system (Ubuntu) are also available from Ospom.com for less than $10. 
   The Python software running on the CPU will be public on Bit Bucket shortly.
* This is an open source-open hardware project, and we welcome participation.
* Install necessary Arduino Libraries using the Library Manager in Arduino v1.5+. (Sketch -> Include Library -> Manage Libraries..).  Install: 'OSPOM', 'EEPROMex'.  'CapacitiveSensor' and 'TLC59108' are necessary for the Reef POM Level Sensor.  'Narcoleptic' is necessary for the Sun Drop.  We also offer these Libraries in our Git Repo in the Source in 'Libraries', as well as in our downloads section.

### Contact Info:
*  Email: Staff@ReefPOM.com
*  Website: www.ReefPOM.com
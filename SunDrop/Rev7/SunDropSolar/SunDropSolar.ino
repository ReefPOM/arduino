/*Sun Drop 7th Order Solar Version IR-Send Only Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

/*
Sun Drop 7th order Pinout
0 -IRrecieve
1 -IRsend
3 -CapSense Fill
4 -CapSense Read
5 -IR Recieve V+ 1k ohm
6 -IR Recieve V+ 500 ohm
7 -Power On for Sensors
8 -IR send LED
9 -Flow (Heated) Power (Thermistor 1)
10 -IR Recieve Reference voltage 3k ohm
11 -IR Recieve Reference voltage 15k ohm
12 -IR Recieve Reference voltage 43k ohm
13 -Flow Heater Pin  *HIGH = ON, LOW = OFF
15 -FLow Reading
16 -PAR Reading
18 -VoltageTest Reading
19 -Temp Reading
*/

#include <CapacitiveSensor.h>
#include <Narcoleptic.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(0, 1); // RX, TX
long capVal = 0;
long capValAvg = 0;
CapacitiveSensor   levelSensor1 = CapacitiveSensor(3,4); //Definng which pins are the (Fill,Sense) pins.

const int IRPwr1kPin = 5;
const int IRPwr500Pin = 6;
const int IRRef3kPin = 10;
const int IRRef15kPin = 11;
const int IRRef43kPin = 12;

const int VReadPin = 18;
const int FlowPwrPin = 9;    // P-mosfet
const int FlowHeatPin = 13;  // N-mosfet
const int FlowReadPin = 15;
const int TempReadPin = 19;
const int PARPin = 16; 
const int PwrOnPin = 7;    // P-mosfet
int Flow;
int FlowAvg;
int Temp;
int TempAvg;
int PAR;
int PARAvg;
int Voltage;
int VoltageAvg;
int input;

void setup() {
  mySerial.begin(4800);   //Software Serial deals better with Narcoleptic
  
  pinMode (IRPwr1kPin, OUTPUT);
  pinMode (IRPwr500Pin, OUTPUT);
  pinMode (IRRef3kPin, OUTPUT);
  pinMode (IRRef15kPin, OUTPUT);
  pinMode (IRRef43kPin, OUTPUT);
  
  pinMode (FlowPwrPin, OUTPUT);
  pinMode (FlowReadPin, INPUT);
  pinMode (TempReadPin, INPUT);
  pinMode (VReadPin, INPUT);
  pinMode (PARPin, INPUT);
  pinMode (PwrOnPin, OUTPUT);

 
  levelSensor1.set_CS_Timeout_Millis(4000);  //setting up cap-sense
  levelSensor1.set_CS_AutocaL_Millis(0xFFFFFFFF);
 
  digitalWrite (IRPwr1kPin, LOW);
  digitalWrite (IRPwr500Pin, LOW);
  digitalWrite (IRRef3kPin, LOW);
  digitalWrite (IRRef15kPin, LOW);
  digitalWrite (IRRef43kPin, LOW);

  digitalWrite(FlowPwrPin, LOW);  //turn on for testing
  digitalWrite(FlowHeatPin, HIGH);  //turn on for testing
  delay(1000);
  analogReference(INTERNAL);
}


// the loop routine runs over and over again forever:
void loop() {
  
    //Turn on power to testing circuits
  digitalWrite(FlowPwrPin, LOW);  //Turn on flow thermistor
  digitalWrite(FlowHeatPin, LOW);  //Turn off flow heater for testing
  digitalWrite(PwrOnPin, LOW);  //LOW = ON
  delay(10);  //give the sensors time to warm up
 
    //Sensor Reading Section 10x then avg
  for (int i=0; i<10; i++) {
    capVal += levelSensor1.capacitiveSensorRaw(100);  //reading cap-sense
    Flow += analogRead(FlowReadPin);  //Flow Read
    Temp += analogRead(TempReadPin);  //Thermistor 2 Read
    PAR += analogRead(PARPin);  //PAR Section
    Voltage += analogRead(VReadPin);  //Voltage Reading Section
  }
    //Averaging the Sensor Readings
  capValAvg = capVal / 10;
  capVal = 0;
  FlowAvg = Flow / 10;
  Flow = 0;
  TempAvg = Temp / 10;
  Temp = 0;
  PARAvg = PAR / 10;
  PAR = 0;
  VoltageAvg = Voltage / 10;
  Voltage = 0;  

  //IR Send Section
  mySerial.println("00000004");
  mySerial.println(capValAvg);
  mySerial.println(PARAvg);
  mySerial.println(FlowAvg);   
  mySerial.println(TempAvg);
  mySerial.println(VoltageAvg);

  //Put the SunDrop to sleep for 30 seconds
  digitalWrite(FlowPwrPin, HIGH);  //HIGH = OFF
  digitalWrite(PwrOnPin, HIGH);  //HIGH = OFF
  Narcoleptic.delay(30000);
}

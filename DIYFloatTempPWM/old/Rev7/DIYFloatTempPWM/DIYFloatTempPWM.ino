/* DIY Float Temp PWM board 7th Order Ai Speak
 *   (AKA The Ryan)
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 */

const int PWM1Pin = 3;
const int PWM2Pin = 5;
const int PWM3Pin = 6;
const int PWM4Pin = 9;
const int PWM5Pin = 10;
const int PWM6Pin = 11;

const int Temp1Pin = 20;
const int Temp2Pin = 21;
const int Temp3Pin = 14;
const int Temp4Pin = 19;
const int Temp5Pin = 18;
const int Temp6Pin = 17;
const int Temp7Pin = 16;
const int Temp8Pin = 15;

const int Float1Pin = 4;
const int Float2Pin = 2;
const int Float3Pin = 7;
const int Float4Pin = 8;
const int Float5Pin = 13;
const int Float6Pin = 12;

//const int VoltagePin = ?;  Not Available on 7th order


int PWM1 = 0;
int PWM2 = 0;
int PWM3 = 0;
int PWM4 = 0;
int PWM5 = 0;
int PWM6 = 0;

int Temp1Avg = 0;
int Temp1f = 0;
int Temp1c = 0;
int Temp2Avg = 0;
int Temp2f = 0;
int Temp2c = 0;
int Temp3Avg = 0;
int Temp3f = 0;
int Temp3c = 0;
int Temp4Avg = 0;
int Temp4f = 0;
int Temp4c = 0;
int Temp5Avg = 0;
int Temp5f = 0;
int Temp5c = 0;
int Temp6Avg = 0;
int Temp6f = 0;
int Temp6c = 0;
int Temp7Avg = 0;
int Temp7f = 0;
int Temp7c = 0;
int Temp8Avg = 0;
int Temp8f = 0;
int Temp8c = 0;

int Float1Val = 0;
int Float2Val = 0;
int Float3Val = 0;
int Float4Val = 0;
int Float5Val = 0;
int Float6Val = 0;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int actuatorVal = 0;
int Comand = 0;
int Comand1 = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
unsigned long BeginTime = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String ID = "gry00000";   // <********** SET GROUP ID HERE *************
String IDIn = "";
boolean Pause = false;

void setup() {
  Serial.begin(57600);
    // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  }
  
void loop() {
    
TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("etf00001:");  //Temp in Farenheight
      Serial.print(Temp1f);
      Serial.print(",");
      Serial.print("etc00001:");  //Temp in Celcius
      Serial.print(Temp1c);
      Serial.print(",");
      Serial.print("etf00002:");  //Temp in Farenheight
      Serial.print(Temp2f);
      Serial.print(",");
      Serial.print("etc00002:");  //Temp in Celcius
      Serial.print(Temp2c);
      Serial.print(",");
      Serial.print("etf00003:");  //Temp in Farenheight
      Serial.print(Temp3f);
      Serial.print(",");
      Serial.print("etc00003:");  //Temp in Celcius
      Serial.print(Temp3c);
      Serial.print(",");
      Serial.print("etf00004:");  //Temp in Farenheight
      Serial.print(Temp4f);
      Serial.print(",");
      Serial.print("etc00004:");  //Temp in Celcius
      Serial.print(Temp4c);
      Serial.print(",");
      Serial.print("etf00005:");  //Temp in Farenheight
      Serial.print(Temp5f);
      Serial.print(",");
      Serial.print("etc00005:");  //Temp in Celcius
      Serial.print(Temp5c);
      Serial.print(",");
      Serial.print("etf00006:");  //Temp in Farenheight
      Serial.print(Temp6f);
      Serial.print(",");
      Serial.print("etc00006:");  //Temp in Celcius
      Serial.print(Temp6c);
      Serial.print(",");
      Serial.print("etf00007:");  //Temp in Farenheight
      Serial.print(Temp7f);
      Serial.print(",");
      Serial.print("etc00007:");  //Temp in Celcius
      Serial.print(Temp7c);
      Serial.print(",");
    /*  Serial.print("etf00008:");  //Not available on 7th order
      Serial.print(Temp8f);
      Serial.print(",");
      Serial.print("etc00008:");  
      Serial.print(Temp8c);
      Serial.print(",");     */
      Serial.print("efs00001:");  //Float Switch Value
      Serial.print(Float1Val);
      Serial.print(",");
      Serial.print("efs00002:");  //Float Switch Value
      Serial.print(Float2Val);
      Serial.print(",");
      Serial.print("efs00003:");  //Float Switch Value
      Serial.print(Float3Val);
      Serial.print(",");
      Serial.print("efs00004:");  //Float Switch Value
      Serial.print(Float4Val);
      Serial.print(",");
      Serial.print("efs00005:");  //Float Switch Value
      Serial.print(Float5Val);
      Serial.print(",");
      Serial.print("efs00006:");  //Float Switch Value
      Serial.print(Float6Val);
      Serial.print(",");
    /*  Serial.print("evo00001:");    //Not available on 7th order
      Serial.println(VoltageCal);  */
      LastSend = millis() - 250;  //The program takes about 250ms to run
    }
  }


      //Decides what to do when it recieves a serial command
  if (stringComplete) {
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
 
      switch (Comand) {
      case 0:
        //read device ID from flash memory
      Serial.print("GroupID");
      Serial.print(ID);
        // clear the string:
        inputString = "";
        stringComplete = false;
        Comand = 0;
        Comand1 = 0;
        break;

      case 1:
        //Send Back Sensor Data
        Serial.print("GroupID");
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("Temp1:");  //Temp in Celcius
      Serial.print(analogRead(Temp1Pin));
      Serial.print(",");
      Serial.print("Temp2:");  //Temp in Celcius
      Serial.print(analogRead(Temp2Pin));
      Serial.print(",");
      Serial.print("Temp3:");  //Temp in Celcius
      Serial.print(analogRead(Temp3Pin));
      Serial.print(",");
      Serial.print("Temp4:");  //Temp in Celcius
      Serial.print(analogRead(Temp4Pin));
      Serial.print(",");
      Serial.print("Temp5:");  //Temp in Celcius
      Serial.print(analogRead(Temp5Pin));
      Serial.print(",");
      Serial.print("Temp6:");  //Temp in Celcius
      Serial.print(analogRead(Temp6Pin));
      Serial.print(",");
      Serial.print("Temp7:");  //Temp in Celcius
      Serial.println(analogRead(Temp7Pin));
    /*  Serial.print("Temp8:");  //Temp in Celcius
      Serial.print(analogRead(Temp8Pin));
      Serial.print(",");  */
      
      Serial.print("Float1:");  //Float Switch Value
      Serial.print(digitalRead(Float1Pin));
      Serial.print(",");
      Serial.print("Float2:");  //Float Switch Value
      Serial.print(digitalRead(Float2Pin));
      Serial.print(",");
      Serial.print("Float3:");  //Float Switch Value
      Serial.print(digitalRead(Float3Pin));
      Serial.print(",");
      Serial.print("Float4:");  //Float Switch Value
      Serial.print(digitalRead(Float4Pin));
      Serial.print(",");
      Serial.print("Float5:");  //Float Switch Value
      Serial.print(digitalRead(Float5Pin));
      Serial.print(",");
      Serial.print("Float6:");  //Float Switch Value
      Serial.println(digitalRead(Float6Pin));
    /*  Serial.print("evo00001:");    //Not available on 7th order
      Serial.println(VoltageCal);  */
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 2:
        //Set PWM #1 pin
        PWM1 = calcfunction(inputString);
        PWM1 = PWM1 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM1Pin, LOW);
          Serial.println("1");  
        }
        else {
          analogWrite(PWM1Pin, PWM1);
          Serial.println("1");  
        }
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 3:
        //Set PWM #2 pin
        PWM2 = calcfunction(inputString);
        PWM2 = PWM2 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM2 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM2Pin, LOW);
          Serial.println("1");  
        }
        else {
          analogWrite(PWM2Pin, PWM2);
        Serial.println("1");
        }
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 4:
        //Set PWM #3 pin  
        PWM3 = calcfunction(inputString);
        PWM3 = PWM3 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM3Pin, PWM3);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 5:
        //Set PWM #4 pin    
        PWM4 = calcfunction(inputString);
        PWM4 = PWM4 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM4Pin, PWM4);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;

      case 6:
        //Set PWM #5
        PWM5 = calcfunction(inputString);
        PWM5 = PWM5 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM5Pin, PWM5);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;

      case 7:
        //Set PWM #6
        PWM6 = calcfunction(inputString);
        PWM6 = PWM6 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM6Pin, PWM6);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
      case 10: 
         //Send Back Sensor Data to Ai
         Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("etf00001:");  //Temp in Farenheight
      Serial.print(Temp1f);
      Serial.print(",");
      Serial.print("etc00001:");  //Temp in Celcius
      Serial.print(Temp1c);
      Serial.print(",");
      Serial.print("etf00002:");  //Temp in Farenheight
      Serial.print(Temp2f);
      Serial.print(",");
      Serial.print("etc00002:");  //Temp in Celcius
      Serial.print(Temp2c);
      Serial.print(",");
      Serial.print("etf00003:");  //Temp in Farenheight
      Serial.print(Temp3f);
      Serial.print(",");
      Serial.print("etc00003:");  //Temp in Celcius
      Serial.print(Temp3c);
      Serial.print(",");
      Serial.print("etf00004:");  //Temp in Farenheight
      Serial.print(Temp4f);
      Serial.print(",");
      Serial.print("etc00004:");  //Temp in Celcius
      Serial.print(Temp4c);
      Serial.print(",");
      Serial.print("etf00005:");  //Temp in Farenheight
      Serial.print(Temp5f);
      Serial.print(",");
      Serial.print("etc00005:");  //Temp in Celcius
      Serial.print(Temp5c);
      Serial.print(",");
      Serial.print("etf00006:");  //Temp in Farenheight
      Serial.print(Temp6f);
      Serial.print(",");
      Serial.print("etc00006:");  //Temp in Celcius
      Serial.print(Temp6c);
      Serial.print(",");
      Serial.print("etf00007:");  //Temp in Farenheight
      Serial.print(Temp7f);
      Serial.print(",");
      Serial.print("etc00007:");  //Temp in Celcius
      Serial.print(Temp7c);
      Serial.print(",");
    /*  Serial.print("etf00008:");  //Not available on 7th order
      Serial.print(Temp8f);
      Serial.print(",");
      Serial.print("etc00008:");  
      Serial.print(Temp8c);
      Serial.print(",");     */
      Serial.print("efs00001:");  //Float Switch Value
      Serial.print(Float1Val);
      Serial.print(",");
      Serial.print("efs00002:");  //Float Switch Value
      Serial.print(Float2Val);
      Serial.print(",");
      Serial.print("efs00003:");  //Float Switch Value
      Serial.print(Float3Val);
      Serial.print(",");
      Serial.print("efs00004:");  //Float Switch Value
      Serial.print(Float4Val);
      Serial.print(",");
      Serial.print("efs00005:");  //Float Switch Value
      Serial.print(Float5Val);
      Serial.print(",");
      Serial.print("efs00006:");  //Float Switch Value
      Serial.print(Float6Val);
      Serial.print(",");
    /*  Serial.print("evo00001:");    //Not available on 7th order
      Serial.println(VoltageCal);  */
         // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;

      case 12:
         //DImming on and off
        analogWrite(PWM1Pin, PWM1);
        analogWrite(PWM2Pin, PWM2);
        analogWrite(PWM3Pin, PWM3);
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;

      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
      } 
    } 
    
    else {
      //Read Temp, FloatSwitches, average temps
      Temp1Avg = 0;
      Temp2Avg = 0;
      Temp3Avg = 0;
      Temp4Avg = 0;
      Temp5Avg = 0;
      Temp6Avg = 0;
      Temp7Avg = 0;
      //Temp8Avg = 0;   Not Available on 7th order
      //VoltageAvg = 0;  Not Available on 7th order
      
      for (int i = 0; i < 10; i++) {
        Temp1Avg += analogRead(Temp1Pin);
        Temp2Avg += analogRead(Temp2Pin);
        Temp3Avg += analogRead(Temp3Pin);
        Temp4Avg += analogRead(Temp4Pin);
        Temp5Avg += analogRead(Temp5Pin);
        Temp6Avg += analogRead(Temp6Pin);
        Temp7Avg += analogRead(Temp7Pin);
      //  Temp8Avg += analogRead(Temp8Pin);   Not Available on 7th Order
      //  VoltageAvg += analogRead(VoltagePin);   Not Available on 7th Order
        }
      Temp1f = Temp1Avg / 10;
      Temp1f = Temp1f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp1f = Temp1f / 4.4138; //y=4.4138x+179.3788
      Temp1c = Temp1f - 32;      //Calculate 
      Temp1c = Temp1c * 5 / 9 ;  //Celcius
      Temp2f = Temp2Avg / 10;
      Temp2f = Temp2f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp2f = Temp2f / 4.4138; //y=4.4138x+179.3788
      Temp2c = Temp2f - 32;      //Calculate 
      Temp2c = Temp2c * 5 / 9 ;  //Celcius
      Temp3f = Temp3Avg / 10;
      Temp3f = Temp3f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp3f = Temp3f / 4.4138; //y=4.4138x+179.3788
      Temp3c = Temp3f - 32;      //Calculate 
      Temp3c = Temp3c * 5 / 9 ;  //Celcius
      Temp4f = Temp4Avg / 10;
      Temp4f = Temp4f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp4f = Temp4f / 4.4138; //y=4.4138x+179.3788
      Temp4c = Temp4f - 32;      //Calculate 
      Temp4c = Temp4c * 5 / 9 ;  //Celcius
      Temp5f = Temp5Avg / 10;
      Temp5f = Temp5f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp5f = Temp5f / 4.4138; //y=4.4138x+179.3788
      Temp5c = Temp5f - 32;      //Calculate 
      Temp5c = Temp5c * 5 / 9 ;  //Celcius
      Temp6f = Temp6Avg / 10;
      Temp6f = Temp6f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp6f = Temp6f / 4.4138; //y=4.4138x+179.3788
      Temp6c = Temp6f - 32;      //Calculate 
      Temp6c = Temp6c * 5 / 9 ;  //Celcius
      Temp7f = Temp7Avg / 10;
      Temp7f = Temp7f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp7f = Temp7f / 4.4138; //y=4.4138x+179.3788
      Temp7c = Temp7f - 32;      //Calculate 
      Temp7c = Temp7c * 5 / 9 ;  //Celcius
   /*   Temp1Val = Temp8Avg / 10;    Not Available on 7th order
      Temp8f = Temp8f - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Temp8f = Temp8f / 4.4138; //y=4.4138x+179.3788
      Temp8c = Temp8f - 32;      //Calculate 
      Temp8c = Temp8c * 5 / 9 ;  //Celcius   
      VoltageVal = VoltageAvg / 10;  
      VoltageVal = VoltageVal - 1408;  //Calibrating Voltage
      VoltageVal = VoltageVal / -144.5;  //Y=-140.8x+1408 (if vref is 3.3)  */
      
      Float1Val = digitalRead(Float1Pin);
      Float2Val = digitalRead(Float2Pin);
      Float3Val = digitalRead(Float3Pin);
      Float4Val = digitalRead(Float4Pin);
      Float5Val = digitalRead(Float5Pin);
      Float6Val = digitalRead(Float6Pin);
      }
    
}
    
    
    
    
    
    
      //Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = inputString.charAt(i);
        IDIn += IDRead;
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        IDIn = "";
      } 
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(ID);
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 9 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}

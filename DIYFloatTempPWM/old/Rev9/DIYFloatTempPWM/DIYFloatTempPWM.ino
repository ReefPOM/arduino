/*  Reef POM DIY Float Temp PWM v8 & v9   (AKA The Ryan)
 *   
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *

Arduino Pinout: 
Pin   Function
--------------
2     Float2Pin
11     PWM1Pin
4     Float1Pin
10     PWM2Pin
9     PWM3Pin
7     Float3Pin
8     Float4Pin
6     PWM4Pin
5     PWM5Pin
3     PWM6Pin 
12    Float6Pin
13    Float5Pin
14    DIYTemp3Pin
15    DIYTemp8Pin
16    DIYTemp7Pin
17    DIYTemp6Pin
18    DIYTemp5Pin
19    DIYTemp4Pin
20    DIYTemp1Pin
21    DIYTemp2Pin

Ryan Hub Element List:
Element, Function, Pin#
------------------------
1     , Float#1   , 4    *One Wire Temp Sensor
2     , Float#2   , 2    *Float Switch
3     , Float#3   , 7    *Float Switch
4     , Float#4   , 8
5     , Float#5   , 13
6     , Float#6   , 12
7     , Temp#1    , 20
8     , Temp#2    , 21
9     , Temp#3    , 14
10    , Temp#4    , 19
11    , Temp#5    , 18
12    , Temp#6    , 17
13    , Temp#7    , 16
14    , Temp#8    , 15
15    , PWM#1     , 11
16    , PWM#2     , 10
17    , PWM#3     , 9
18    , PWM#4     , 6
19    , PWM#5     , 5
20    , PWM#6     , 3
*/


#include <OspomLite.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
#include <OneWire.h>   // DS18S20 Temperature chip i/o
//#include <CapacitiveSensor.h>
OspomLite ospomLite;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable
//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

OneWire ds(4);  // <--- Set Pin Here   & set element number lower down.
unsigned long previousMillis = 0;

void setup() {  // put your setup code here, to run once:
  ospomLite.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospomLite.Run(1000);  //Activates the ospom library every time the loop runs **This is required
        //Delay in milliseconds between reads (333 = Read Sensors 3 times a second, 1000 = 1 time a second.)
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    ospom.Set(element Number(int)), value(float));  = Send your sensor reading to an ospom element so it is displayed on the dashboard
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */

  //One Wire Temperature Probe reading section.  This can be refactored.
  int HighByte, LowByte, TReading;
  float Tc_100;
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  ds.reset_search();
  if ( !ds.search(addr)) {
//      Serial.print("No more addresses.\n");
//      ds.reset_search();
//      return;
  }
  ds.reset();
  ds.select(addr);      //SET TO 12 BIT AROUND HERE 
  ds.write(0x44,1);         // start conversion, with parasite power on at the end
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= 1000) {
    // save the last time
    previousMillis = currentMillis;   
    present = ds.reset();
    ds.select(addr);    
    ds.write(0xBE);         // Read Scratchpad

    for ( i = 0; i < 9; i++) {           // we need 9 bytes
      data[i] = ds.read();
    }

    LowByte = data[0];
    HighByte = data[1];
    TReading = (HighByte << 8) + LowByte;
    Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25
    float Tc = Tc_100 / 100;
    float Tf = Tc * 1.8; 
    Tf = Tf + 32;
    ospomLite.Set(1, Tf);  //  <-Set element number here
  }
}




/*  Reef POM HumidityNtemp 
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
---------------
18 ,  SDA    ,  I2C for Humidity
19 ,  SCL    ,  I2C for Humidity   

HumidityNtemp Element List
Element, Function, Pin#
1  ,  TempF      ,  I2C
2  ,  Humidity   ,  I2C
3  ,  LED Top    ,  I2C
4  ,  LED Left   ,  I2C
5  ,  LED Center ,  I2C
6  ,  LED Right  ,  I2C
7  ,  LED Bottom ,  I2C
*/



#include <HTU21D.h>
#include <Wire.h>
#include <OspomLite.h>  //include the OspomLite library
#include <EEPROMex.h>  //This must be included for OspomLite to work

HTU21D myHumidity;  //Create an instance of the object
OspomLite ospomLite;  // instantiate ospomLite, an instance of ospomLite Library  Don't put () if it isn't getting a variable

unsigned long previousMillis = 0;

void setup()

{
  ospomLite.Setup();  //ospomLite.Setup initilizes ospomLite 
  myHumidity.begin();
}



void loop() {
  ospomLite.Run(1000);  //Activates the ospomLite library every time the loop runs **This is required
        //Delay in milliseconds between reads (333 = Read Sensors 3 times a second, 1000 = 1 time a second.)
    /*
    Put any standard arduino code in here that you want to run over and over again.
    ospomLite Functions:
    ospomLite.define(Pin Number, Pin Type, PinFunction, PinID);  = Define ospomLite Internet Dashboard accessable Sensor or Actuator Elements
               example: ospomLite.define(7, 's', ospomLite.anaRead, "stfw0000");
    ospomLite.read();  = Read ospomLite Sensors or Actuators and recieve the result
    ospomLite.write();  = Write ospomLite Sensors or Actuators using this function
       **Writing a pin that is part of the ospomLite dashboard without using this function will cause
           the dashboard to be confused
    ospomLite.Set(element Number(int)), value(float));  = Send your sensor reading to an ospomLite element so it is displayed on the dashboard
    
    **Dont forget to Write your own ospomLite Configuation Sketch to setup your arduino, 
       or run the ospomLite Generic Configuration sketch at least once before calling any ospomLite functions
    */

unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= 1000) {
    previousMillis = currentMillis;     // save the last time
    float humd = myHumidity.readHumidity();
    float temp = myHumidity.readTemperature();
    temp = temp * 1.8;
    temp = temp + 32;
    ospomLite.Set(1, temp);
    ospomLite.Set(2, humd);
  }

}

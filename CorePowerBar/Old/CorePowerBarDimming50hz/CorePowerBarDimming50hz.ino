/* CorePowerBar Ai Speak Dimming for 50Hz electricity
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
 
Arduino Pinout: 
2 AC Phase Input
11 Traic #1 (new)
3 Triac #2
4 Triac #3
5 0-10v #1
6 0-10v #4
7 Triac #4
8 Bottom LED
9 0-10v #2
10 0-10v #3
ADC6 Thermisor (new)
ADC7 pH Reading
14 Right LED
15 Center LED
16 Left LED
17 Top LED
18 DIY Pin #2 (SDA)
19 DIY Pin #1 (SCL)
*/

const int PhasePin = 2;
volatile int ZeroCross = 0;
volatile unsigned long ZeroCrossTime = 0;
volatile unsigned long LastCrossTime = 0;
volatile unsigned long LastTriac1Pulse = 0;
volatile unsigned long LastTriac2Pulse = 0;
volatile unsigned long LastTriac3Pulse = 0;
volatile unsigned long LastTriac4Pulse = 0;
volatile unsigned long TimeNowMicro = 0;
unsigned long LastAnalogRead = 0;

int i = 0;

String inputString = "";         // a string to hold incoming data

boolean stringComplete = false;  // whether the string is complete

const int pHPin = A7;
float pHValue = 0;

const int TempPin = A6;
int Tempf = 0;
int Tempc = 0;

int Comand = 0;
int Comand1 = 0;
int actuatorVal = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String ID = "gcp00002";  // <********** SET GROUP ID HERE *************
String IDIn = "";
boolean Pause = false;

float pHAvg = 0;
float TempAvg = 0;

const int TopLEDPin = 17;
const int BottomLEDPin = 8;
const int LeftLEDPin = 16;
const int RightLEDPin = 14;
const int CenterLEDPin = 15;

const int ZTtenPin1 = 5;
int ZTten1 = 0;
const int ZTtenPin2 = 9;
int ZTten2 = 0;
const int ZTtenPin3 = 10;
int ZTten3 = 0;
const int ZTtenPin4 = 6;
int ZTten4 = 0;

const int TriacPin1 = 11;
int Triac1Delay = 0;
int Triac1DelayMicro = 0;
const int TriacPin2 = 3;
int Triac2Delay = 0;
int Triac2DelayMicro = 0;
const int TriacPin3 = 4;
int Triac3Delay = 0;
int Triac3DelayMicro = 0;
const int TriacPin4 = 7;
int Triac4Delay = 0;
int Triac4DelayMicro = 0;

boolean Dim1 = false;
boolean Dim2 = false;
boolean Dim3 = false;
boolean Dim4 = false;

void setup() {
  Serial.begin(57600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);

  pinMode(PhasePin, INPUT);

  pinMode(TopLEDPin, OUTPUT);
  pinMode(BottomLEDPin, OUTPUT);
  pinMode(LeftLEDPin, OUTPUT);
  pinMode(RightLEDPin, OUTPUT);
  pinMode(CenterLEDPin, OUTPUT);

  pinMode(ZTtenPin1, OUTPUT);
  pinMode(ZTtenPin2, OUTPUT);
  pinMode(ZTtenPin3, OUTPUT);
  pinMode(ZTtenPin4, OUTPUT); 

  pinMode(TriacPin1, OUTPUT);
  pinMode(TriacPin2, OUTPUT);
  pinMode(TriacPin3, OUTPUT);
  pinMode(TriacPin4, OUTPUT);
  //Set all other pins as Input so they are safe

  pinMode(2, INPUT);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
  pinMode(18, INPUT);
  pinMode(19, INPUT);      
}

// the loop routine runs over and over again forever:
void loop() {

//Dimming outlet controll section
  ZeroCross = digitalRead(PhasePin);  //Check for a Zero Cross
  TimeNowMicro = micros();  //Read the time right now
  if (ZeroCross) {
    ZeroCrossTime = TimeNowMicro;  //If the sign wave crossed 0v set it to time time now
  }
  if ((ZeroCross) && (ZeroCrossTime > LastCrossTime + 8433)) {  //avoids repeat ZeroCross measurements
    LastCrossTime = ZeroCrossTime;                             //There are 10,000micro seconds in half a sign wave @ 50Hz
  }
  if ((Dim1) && (TimeNowMicro > LastCrossTime + Triac1DelayMicro) && (TimeNowMicro < LastCrossTime + 9638) && (TimeNowMicro > LastTriac1Pulse + 9638 - Triac1DelayMicro)) {
    digitalWrite(TriacPin1, HIGH);   //Turn on the Triac if it's time, and you havent already just done it
    LastTriac1Pulse = TimeNowMicro;
  }
  if ((Dim2) && (TimeNowMicro > LastCrossTime + Triac2DelayMicro) && (TimeNowMicro < LastCrossTime + 9638) && (TimeNowMicro > LastTriac2Pulse + 9638 - Triac2DelayMicro)) {
    digitalWrite(TriacPin2, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac2Pulse = TimeNowMicro;
  }
  if ((Dim3) && (TimeNowMicro > LastCrossTime + Triac3DelayMicro) && (TimeNowMicro < LastCrossTime + 9638) && (TimeNowMicro > LastTriac3Pulse + 9638 - Triac3DelayMicro)) {
    digitalWrite(TriacPin3, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac3Pulse = TimeNowMicro;
  }
  if ((Dim4) && (TimeNowMicro > LastCrossTime + Triac4DelayMicro) && (TimeNowMicro < LastCrossTime + 9638) && (TimeNowMicro > LastTriac4Pulse + 9638 - Triac4DelayMicro)) { 
    digitalWrite(TriacPin4, HIGH);  //Turn on the Triac if it's time, and you havent already just done it
    LastTriac4Pulse = TimeNowMicro;
  }
 
    //Turn off Triacs so they don't stick on at zero cross
  if (TimeNowMicro > LastCrossTime + 8433) {
    digitalWrite(TriacPin1, LOW);
    digitalWrite(TriacPin2, LOW);
    digitalWrite(TriacPin3, LOW);
    digitalWrite(TriacPin4, LOW);
  } 
  
//Measure pH and case temp once a second, then average them
  TimeNow = millis();  //Check the time
  if (TimeNow > LastAnalogRead + 1000) {  //Read pH & Temp once a second
        //Read pH and Temperature, average them
      pHAvg += analogRead(pHPin);
      TempAvg += analogRead(TempPin);
      i++;
      LastAnalogRead = millis();
      
      if (i >= 10) {  //Once every ten seconds average the Temp and pH readings
      i = 0;
      pHValue = pHAvg / 10;
      pHValue = pHValue + 362;  //Calibrating pH
      pHValue = pHValue / 117;  //y=117x-362
      Tempf = TempAvg / 10;
      Tempf = Tempf - 178.3788;  //Convert to Farenheight (subtracted 1 Feb4th)
      Tempf = Tempf / 4.4138; //y=4.4138x+179.3788
      Tempc = Tempf - 32;      //Calculate 
      Tempc = Tempc * 5 / 9 ;  //Celcius
      pHAvg = 0;  //Reset Averages
      TempAvg = 0;
      }
  }


//See if it needs to send a continuous data stream for 60s, and do so
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("eph00000:");
        Serial.print(pHValue);
        Serial.print(",");
        Serial.print("etf00003:");
        Serial.print(Tempf);
        Serial.print(",");
        Serial.print("etc00003:");
        Serial.println(Tempc);
      LastSend = millis();
    }
  }  


//Deals with incoming Serial Requests
    if (stringComplete) {
    digitalWrite(CenterLEDPin, HIGH);
    delay (10);
    digitalWrite(CenterLEDPin, LOW);
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
    
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.println("00000000");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
      
      case 1:
         //Send Back Sensor Data
        Serial.print(ID);  // Group ID
        Serial.print("/");
        Serial.print("pH,");
        Serial.print(pHValue);
        Serial.print(",");
        Serial.print("Tempf,");
        Serial.print(Tempf);
        Serial.print(",");
        Serial.print("Tempc,");
        Serial.println(Tempc);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 2:
        //Set 0-10v #1 pin
        ZTten1 = calcfunction(inputString);
        ZTten1 = ZTten1 * 255 / 100;  //convert to 0-255 for analog write
        if (ZTten1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(ZTtenPin1, LOW);
          Serial.println("1");  
        }
        else {
          analogWrite(ZTtenPin1, ZTten1);
          Serial.println("1");  
        }
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 3:
        //Set 0-10v #2 pin
        ZTten2 = calcfunction(inputString);
        ZTten2 = ZTten2 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin2, ZTten2);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 4:
        //Set 0-10v #3 pin  
        ZTten3 = calcfunction(inputString);
        ZTten3 = ZTten3 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin3, ZTten3);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 5:
        //Set 0-10v #4 pin    
        ZTten4 = calcfunction(inputString);
        ZTten4 = ZTten4 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(ZTtenPin4, ZTten4);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;

      case 6:
        //Set Triac #1 ON, OFF, or Dimmed
        Triac1Delay = calcfunction(inputString);
        if (Triac1Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac1DelayMicro = 9638;
          Serial.println("1");
          Dim1 = false;  
        }
        else if (Triac1Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin1, HIGH);
          Serial.println("1"); 
          Triac1DelayMicro = 0;
          Dim1 = true;
        }
        else if ((Triac1Delay < 100) && (Triac1Delay > 0)) {  
          Triac1DelayMicro = 96 * Triac1Delay;  //convert to Delay:
          Triac1DelayMicro = 9638 - Triac1DelayMicro;  // MilliDelay=9638-(96*%Dim)
          Dim1 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 7:
        //Set Triac #2 ON, OFF, or Dimmed
        Triac2Delay = calcfunction(inputString);
        if (Triac2Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin2, LOW);
          Triac2DelayMicro = 9638;
          Serial.println("1");
          Dim2 = false;  
        }
        else if (Triac2Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin2, HIGH);
          Serial.println("1"); 
          Triac2DelayMicro = 0;
          Dim2 = true;
        }
        else if ((Triac2Delay < 100) && (Triac2Delay > 0)) {  
          Triac2DelayMicro = 96 * Triac2Delay;  //convert to Delay:
          Triac2DelayMicro = 9638 - Triac2DelayMicro;  // MilliDelay=9638-(96*%Dim)
          Dim2 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 8:
        //Set Triac #3 ON, OFF, or Dimmed
        Triac3Delay = calcfunction(inputString);
        if (Triac3Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin3, LOW);
          Triac4DelayMicro = 9638;
          Serial.println("1");
          Dim4 = false;  
        }
        else if (Triac3Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin3, HIGH);
          Serial.println("1"); 
          Triac3DelayMicro = 0;
          Dim3 = true;
        }
        else if ((Triac3Delay < 100) && (Triac3Delay > 0)) {  
          Triac3DelayMicro = 96 * Triac3Delay;  //convert to Delay:
          Triac3DelayMicro = 9638 - Triac3DelayMicro;  // MilliDelay=9638-(96*%Dim)
          Dim3 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;  

      case 9:
        //Set Triac #4 ON, OFF, or Dimmed
        Triac4Delay = calcfunction(inputString);
        if (Triac4Delay == 0) {    //If Off, set Off normally
          digitalWrite(TriacPin4, LOW);
          Triac4DelayMicro = 9638;
          Serial.println("1");
          Dim4 = false;  
        }
        else if (Triac4Delay == 100) {  //If On, ser On normally
          digitalWrite(TriacPin4, HIGH);
          Serial.println("1"); 
          Triac4DelayMicro = 0;
          Dim4 = true;
        }
        else if ((Triac4Delay < 100) && (Triac4Delay > 0)) {  
          Triac4DelayMicro = 96 * Triac4Delay;  //convert to Delay:
          Triac4DelayMicro = 9638 - Triac4DelayMicro;  // MilliDelay=9638-(96*%Dim)
          Dim4 = true;
          Serial.println("1"); 
        }
        else {
          Serial.println("0");
        }
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;


      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
      } 
    } 
}


//Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = inputString.charAt(i);
        IDIn += IDRead;
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        IDIn = "";
      }   //int z = inputString.charAt(0) - '0'; //converting char 2 to int
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(ID);
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 2 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}



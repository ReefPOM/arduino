#!/usr/bin/env python

# sends a serial '!' to the arduino to request uncalibrated sensor data every 10 seconds
# converts the ardiono response to corrected pH and Fahrenheit
# writes the values to 'ph.csv' and 'temp.csv'

import serial, time

ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, bytesize=8, parity='N', stopbits=1)


while(1):
    ser.write('!')
    response = ser.readline()
    print 'arduino response: ' + response.strip()
    rawph = float(response.split(',')[0])
    print 'raw pH value = ' + str(rawph)
    rawtemp = float(response.split(',')[1])
    print 'raw temperature value = ' + str(rawtemp)
    # pH calibration
    phval = (rawph + 362) / 117

    # Fahrenheit calibration
    tempval = (rawtemp - 178.3788) / 4.4138;


    f = open('ph.csv', 'a')
    f.write(str(phval) + '\n')
    f.close()
    print 'wrote: ' + str(phval) + ' to ph.csv'

    f = open('temp.csv', 'a')
    f.write(str(phval) + '\n')
    f.close()
    print 'wrote: ' + str(tempval) + ' to temp.csv'
    print ''
    time.sleep(10)

/*  Reef POM Probe Hub v7
 *   
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *

Probe Hub Arduino Pinout: 
Pin   Function
--------------
Arduino Pinout:
2 DIY Pin Int0
3 DIY Pin Int1
5 LED (pwm)
14 pH #2
15 3.3v (test to find board voltage)
16 pH #1
17 Salinity
18 DIY Pin SDA
19 DIY Pin SCL
A6 DIY Pin Analog
A7 DIY Pin Analog

ProbeHub Element List
Element, Function, Pin#
------------------------
1  ,  pH 1        ,  16
2  ,  pH 2        ,  14
3  ,  Salinity    ,  17
4  ,  pcb voltage ,  15
5  ,  DIY 1       ,  20
6  ,  DIY 2       ,  21
7  ,  DIY 3       ,  18
8  ,  DIY 4       ,  19
9 ,  DIY 5       ,  2
10 ,  DIY 6       ,  3
11 ,  DIY 7       ,  11
12 ,  DIY 9       ,  12
13 ,  LED Val     ,  5
*/

#include <OspomLite.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
//#include <CapacitiveSensor.h>
OspomLite ospomLite;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable

//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

void setup() {  // put your setup code here, to run once:
  ospomLite.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospomLite.Run(1000);  //Activates the ospom library every time the loop runs **This is required
        //(333 = Read Sensors 3 times a second, 1000 = 1 time a second, )
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */
}

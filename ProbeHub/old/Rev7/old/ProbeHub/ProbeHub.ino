/*  Reef Probe Hub Ai Speak 7th Order
 * 10PowerBar Ai Speak Dimming
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
Arduino Pinout:
2 DIY Pin Int0
3 DIY Pin Int1
5 LED (pwm)
14 pH #2
15 3.3v (test to find board voltage)
16 pH #1
17 Salinity
18 DIY Pin SDA
19 DIY Pin SCL
A6 DIY Pin Analog
A7 DIY Pin Analog
*/

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int Comand = 0;
int Comand1 = 0;
int actuatorVal = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String ID = "gph00001";  // <********** SET GROUP ID HERE *************
String IDIn = "";
boolean Pause = false;

int LEDVal = 0;
float SalinVal = 0;
int SalinAvg = 0;
float SalinCal = 0;
float pH1Val = 0;
int pH1Avg = 0;
float pH1Cal = 0;
float pH2Val = 0;
int pH2Avg = 0;
float pH2Cal = 0;
float VoltageVal = 0;
int VoltageAvg = 0;
int VoltageCal = 0;

const int LEDPin = 5;
const int pH1Pin = 16;  //pH or ORP
const int pH2Pin = 14;  //pH or ORP
const int SalinPin = 17;
const int VoltagePin = 15;

void setup() {
    Serial.begin(57600);
 // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  
  pinMode(pH1Pin, INPUT); 
  pinMode(pH2Pin, INPUT);
  pinMode(SalinPin, INPUT);
  pinMode(VoltagePin, INPUT);
  pinMode(LEDPin, OUTPUT);
  }

void loop() {
  TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("eph00000:");  //pH1  /ORP1
      Serial.print(pH1Val);
      Serial.print(",");
      Serial.print("eph00001:");  //pH2 / ORP2
      Serial.print(pH2Val);
      Serial.print(",");
      Serial.print("esl00001:");  //Salinity
      Serial.print(SalinVal);
      Serial.print(",");
      Serial.print("evo00001:");    //Voltage of Board
      Serial.println(VoltageVal);
      LastSend = millis();
    }
  }


      //Decides what to do when it recieves a serial command
  if (stringComplete) {
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
    
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.print("GroupID");
        Serial.println(ID);
        // clear the string:
        inputString = "";
        stringComplete = false;
        Comand = 0;
        Comand1 = 0;
        break;
      
      case 1:
         //Send Back Sensor Data
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("eph00000:");  //pH1  /ORP1
      Serial.print(pH1Avg);
      Serial.print(",");
      Serial.print("eph00001:");  //pH2 / ORP2
      Serial.print(pH2Avg);
      Serial.print(",");
      Serial.print("esl00001:");  //Salinity
      Serial.print(SalinAvg);
      Serial.print(",");
      Serial.print("evo00001:");    //Voltage of Board
      Serial.println(VoltageVal);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 2:
        //Turn ON/Off/Dim LED
        LEDVal = calcfunction(inputString);  //Grab the dimmed value
        LEDVal = LEDVal * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(LEDPin, LEDVal);  // pwm LED analogWrite values from 0 to 255
        Serial.println("LED ON");
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 10: 
         //Send Back Sensor Data to Ai
        Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print("eph00000:");  //pH1  /ORP1
      Serial.print(pH1Val);
      Serial.print(",");
      Serial.print("eph00001:");  //pH2 / ORP2
      Serial.print(pH2Val);
      Serial.print(",");
      Serial.print("esl00001:");  //Salinity
      Serial.print(SalinVal);
      Serial.print(",");
      Serial.print("evo00001:");    //Voltage of Board
      Serial.println(VoltageVal);
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
         
      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;
      
      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
    }
  }

 else {
      //Read Temp, Flow, CapSense, average them
      pH1Avg = 0;
      pH2Avg = 0;
      SalinAvg = 0;
      VoltageAvg = 0;
      
      for (int i = 0; i < 10; i++) {
        pH1Avg += analogRead(pH1Pin);
        pH2Avg += analogRead(pH2Pin);
        SalinAvg += analogRead(SalinPin);
        VoltageAvg += analogRead(VoltagePin);
        }
      pH1Val = pH1Avg / 10;
      pH1Val = pH1Val + 362;  //Calibrating pH
      pH1Val = pH1Val / 117;  //y=117x-362
      pH2Val = pH2Avg / 10;
      pH2Val = pH2Val + 362;  //Calibrating pH
      pH2Val = pH2Val / 117;  //y=117x-362
      SalinVal = SalinAvg / 10;
      //Calibrate!
      VoltageVal = VoltageAvg / 10;  
      VoltageVal = VoltageVal - 1408;  //Calibrating Voltage
      VoltageVal = VoltageVal / -144.5;  //Y=-140.8x+1408 (if vref is 3.3)
      }
}


    //Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = inputString.charAt(i);
        IDIn += IDRead;
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        IDIn = "";
      }   //int z = inputString.charAt(0) - '0'; //converting char 2 to int
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(ID);
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        IDIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 2 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}

/*  Reef Probe Hub Rev.8
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 *
Arduino Pinout:
2 DIY Pin #5
3 DIY Pin #4
5 LED Brightness(pwm)
11 DIY Pin #2 (mosi)
12 DIY Pin #3 (miso)
13 DIY Pin #1 (sck)
14 pH #2
15 3.3v (test to find board voltage)
16 pH #1
17 Salinity
18 DIY Pin #7  SDA
19 DIY Pin #6 SCL
A6 DIY Pin #9 Analog
A7 PCB Temp
*/

#include <EEPROMex.h>  //For Calibration Data and ID

      //*******************************Set These************************************
  const int SensorPins[] = {21,21,16,14,17,15,13,11,12,3,2,19,18,20};//  <--SET THE ARDUINO PINS OF THE SENSORS HERE
  const int ActuatorPins[] = {5};//  <--SET THE ARDUINO PINS OF THE SENSORS HERE
  const int SenorTypes[] = {1,1,1,1,1,1,0,0,0,0,0,0,0,0};   // <---Set sensor type here 0=unused, 1=analogRead, 2=digitalRead, 3=CustomFunction
  const int ActuatorTypes[] = {2};   // <---Set non DIY actuator types here: 1=digitalWrite, 2=analogWrite, 3=CustomFunction
     //******************************************************************************

//Variable needed by all Ai Arduino's:
String InputString = "";
boolean Pause = false;
unsigned long WatchDogTime = 0;
int WatchDogEnable = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
unsigned long LastMeasurement = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
char GroupID[] = "        ";
const int numReadings = 15;  //number of analog readings in array
int index = 0;        // the index of the current analog reading
int numSensors = 0; 
int numActuators = 0;
//********************************Fill these arrays all the way out*******************************************************************************
const int SensorEEPROMIDLocations[] = {8,24,40,56,72,88,104,120,136,152,168,184,200,216};    //  <---The EEPROM address of sensor elements
const int ActuatorEEPROMIDLocations[] = {344};    //  <---The EEPROM address of actuator elements
const int SensorEEPROMCalMLocations[] = {16,32,48,64,80,96,112,128,144,160,176,192,208,224};    //  <---The EEPROM address of Cal M's
const int SensorEEPROMCalBLocations[] = {20,36,52,68,84,100,116,132,148,164,180,196,212,228};    //  <---The EEPROM address of Cal B's
const int ActuatorEEPROMFSLocations[] = {352};    //  <---The EEPROM address of Fail Safe Values


//Data Structure Defining all Sensor Elements
struct SensorElements {
  char SensorID[9] = "        ";  //ID  *when declaring a char array, you add 1 more than the number of chars, 
                                  // but for eeprom do not add the extra one.
  int SensorVal[numReadings];  // the readings from the analog input as an array
  float SensorTotal = 0;       // the running total
  float SensorAvg = 0;    //The average
  float SensorCalVal = 0;    //Calibrated
  float SensorCalMVal = 0;    //Calibration Value M, Slope
  float SensorCalBVal = 0;    //Calibration Value B, Slope
  int SensorEEPROMIDLocation = 0;    //Location of the Element ID in EEPROM
  int SensorEEPROMCalMLocation = 0;  //Location of Calibration M in EEPROM
  int SensorEEPROMCalBLocation = 0;  //Location of Calibration B in EEPROM
  int SensorPin = 0;
  int SensorType = 0;
  };
  
//Data Structure Defining all Actuator Elements
struct ActuatorElements {
  char ActuatorID[9] = "        ";  //ID
  int ActuatorVal = 0;      //The Current Actuator Value
  int ActuatorFSVal = 0;     //Fail Safe Value
  int ActuatorEEPROMIDLocation = 0;    //Location of the Element ID in EEPROM
  int ActuatorEEPROMFSLocation = 0;    //Location of the Fail Safe Values in EEPROM
  int ActuatorPin = 0;
  int ActuatorType = 0;
};
  
//Data Structure Defining Recieved Serial Message
struct serialCommandStruct {
  int ElementNumber = 0;
  int CommandInt = 0;
  char CommandChar = ' ';
  int CommandValInt = 0;
  float CommandValFloat = 0;
  char CommandValCharArray[9] = "        ";
  boolean Group = false;
  boolean Sensor = false;
  boolean Actuator = false;
};

//Defining serialComand struct  
serialCommandStruct serialCommand; 
  
//Array of Sensor Element Structs
SensorElements SensorElementArray[numSensors];
                       
//Array of Actuator Element Structs
ActuatorElements ActuatorElementArray[numActuators];

void setup() {
  Serial.begin(57600);
     // reserve 200 bytes for the inputString:
  InputString.reserve(200);
      
      //Determine the number of Actuators and Sensors


      //Fill the Sensor struct array
  for (int i = 0; i < numSensors; i++) {
        //Add the EEPROM locations & arduino pins to the Sensor Elements Data Struct 
    SensorElementArray[i].SensorPin = SensorPins[i];
    pinMode(SensorElementArray[i].SensorPin, INPUT);  //Set the sensor pins as inputs
    SensorElementArray[i].SensorEEPROMIDLocation = SensorEEPROMIDLocations[i];
    SensorElementArray[i].SensorEEPROMCalMLocation = SensorEEPROMCalMLocations[i];
    SensorElementArray[i].SensorEEPROMCalBLocation = SensorEEPROMCalBLocations[i];
        ////Read Sensor Element ID's & Calibration Data From EEPROM
    EEPROM.readBlock<char>(SensorElementArray[i].SensorEEPROMIDLocation, SensorElementArray[i].SensorID, 8);
    SensorElementArray[i].SensorCalMVal = EEPROM.readFloat(SensorElementArray[i].SensorEEPROMCalMLocation);
    SensorElementArray[i].SensorCalBVal = EEPROM.readFloat(SensorElementArray[i].SensorEEPROMCalBLocation);
  }
  
       //Fill the Actuator struct array
  for (int i = 0; i < numActuators; i++) { 
        //Add the EEPROM locations & arduino pins to the Actuator Elements Data Struct
    ActuatorElementArray[i].ActuatorPin = ActuatorPins[i];
    pinMode(SensorElementArray[i].SensorPin, OUTPUT);  //Set the actuator pins as outputs
    ActuatorElementArray[i].ActuatorEEPROMIDLocation = ActuatorEEPROMIDLocations[i];
    ActuatorElementArray[i].ActuatorEEPROMFSLocation = ActuatorEEPROMFSLocations[i];
        //Read Actuator Element ID's & FailSafe Data From EEPROM
    EEPROM.readBlock<char>(ActuatorElementArray[i].ActuatorEEPROMIDLocation, ActuatorElementArray[i].ActuatorID, 8);
    ActuatorElementArray[i].ActuatorFSVal = EEPROM.readFloat(ActuatorElementArray[i].ActuatorEEPROMFSLocation);
  }
      
   // initialize all the analog readings to 0:
  for (int i = 0; i < numSensors; i++) {
    for (int thisReading = 0; thisReading < numReadings; thisReading++) {
      SensorElementArray[i].SensorVal[thisReading] = 0;
    }
  }
  
      //Assign a new group ID if it can't find it's group ID
   if (EEPROM.read(0) != 'g') {
     EEPROM.updateBlock<char>(0,"00000000", 8);
   }
  WatchDogEnable = EEPROM.readInt(554);  //Watchdog Enable/Disable
  WatchDogTime = millis();  //Delay the watchdog
  EEPROM.readBlock<char>(0, GroupID, 8);
 
      //Ask odroid for Actuator Data at Startup
  Serial.print(GroupID);
  Serial.print("/");
  Serial.println("SendActData");
  }

void loop() {
  TimeNow = millis();  //Check the time
  
      //Organizes 60 second stream of sensor data to Odroid
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
      //Sends 60 second steam of sensor data to odroid
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(GroupID);  //Group ID
        Serial.print("/");
        for (int i = 0; i < numSensors; i++) {   //Prints all the Element ID's and Values one after another
          Serial.print(SensorElementArray[i].SensorID);
          Serial.print(":");
          Serial.print(SensorElementArray[i].SensorCalVal);
          Serial.print(",");
        }
        Serial.println();   //Ends the line
      LastSend = millis();
    }
  }

  if (WatchDogEnable == 1) {
    if (TimeNow > WatchDogTime + 300000) {  //If the arduino hasn't recieved any signal in 5 minutes
        //Initiate Fail-States
      //Triac1 = Triac1FS  
      //blink the leds
      }
    }    

     //Decides what to do when it recieves a group command
  if (serialCommand.Group) {
    switch (serialCommand.CommandInt) {
      case 0:
        //read device ID from flash memory
        Serial.print(GroupID);
        Serial.print("/");
        Serial.println(GroupID);
        break;
      
      case 1:
            //Send Back Raw Sensor Data
        for (int i = 0; i < numSensors; i++) {
          Serial.print(SensorElementArray[i].SensorID);
          Serial.print(":");
          Serial.print(analogRead(SensorElementArray[i].SensorPin));
          Serial.print(",");
        }
        Serial.println();   //Ends the line
        break;  
        
      case 10: 
         //Send Back Calibrated Sensor Data to Ai
        Serial.print(GroupID);  //Group ID
        Serial.print("/");
        for (int i = 0; i < numSensors; i++) {   
          Serial.print(SensorElementArray[i].SensorID);
          Serial.print(":");
          Serial.print(SensorElementArray[i].SensorCalVal);
          Serial.print(",");
        }
        Serial.println();
        break;  
         
      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = serialCommand.CommandValInt;  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
        break;  
      
      case 13:
          //Send Back Actuator Data
        Serial.print(GroupID);  //Group ID
        Serial.print("/");
        for (int i = 0; i < numActuators; i++) {   
          Serial.print(ActuatorElementArray[i].ActuatorID);
          Serial.print(":");
          Serial.print(ActuatorElementArray[i].ActuatorVal);
          Serial.print(",");
        }
        Serial.println();
        break; 
      
      case 17:
        //Enable/Disable Watchdog timer (1/0)
        EEPROM.writeInt(242, serialCommand.CommandValInt);  //Save value to EEPROM
        WatchDogEnable = serialCommand.CommandValInt;
        success(); 
        break; 
        
      case 18:
          //Write Group ID to EEPROM
        EEPROM.writeBlock<char>(0, serialCommand.CommandValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(0, GroupID, 8);
        success();
        break;   

      default:
        fail();
        break;  
    }
    // clear the string:
    clearTheString(); 
  }
  
  else if (serialCommand.Sensor) {
           //Do what the command Character says
    if (serialCommand.CommandChar == 'R') {
         //Read the actuator value, and send it back to the odroid 
      Serial.print(GroupID);
      Serial.print("/");
      Serial.print(SensorElementArray[serialCommand.ElementNumber].SensorID);
      Serial.print(":");
      Serial.println(SensorElementArray[serialCommand.ElementNumber].SensorCalVal);
    }
    else if (serialCommand.CommandChar == 'I') {
         //Change the Elements ID
      EEPROM.updateBlock<char>(SensorElementArray[serialCommand.ElementNumber].SensorEEPROMIDLocation, serialCommand.CommandValCharArray, 8);
      EEPROM.readBlock<char>(SensorElementArray[serialCommand.ElementNumber].SensorEEPROMIDLocation, SensorElementArray[serialCommand.ElementNumber].SensorID, 8);
      success();
    }
    else if (serialCommand.CommandChar == 'M') {
        //Change the Elements Calibration Slope
      EEPROM.updateFloat(SensorElementArray[serialCommand.ElementNumber].SensorEEPROMCalMLocation, serialCommand.CommandValFloat);  //Save value to EEPROM);
      SensorElementArray[serialCommand.ElementNumber].SensorCalMVal = serialCommand.CommandValFloat;
      success();
    }
    else if (serialCommand.CommandChar == 'B') {
        //Change the Elements Calibration Slope
      EEPROM.updateFloat(SensorElementArray[serialCommand.ElementNumber].SensorEEPROMCalBLocation, serialCommand.CommandValFloat);  //Save value to EEPROM);
      SensorElementArray[serialCommand.ElementNumber].SensorCalBVal = serialCommand.CommandValFloat;
      success();
    }
    else {
      fail();  //Return a -1
    }
    clearTheString();  
  }

  else if (serialCommand.Actuator) {
         //Do what the command Character says
    if (serialCommand.CommandChar == 'R') {
         //Read the actuator value, and send it back to the odroid
      Serial.print(GroupID);
      Serial.print("/");
      Serial.print(ActuatorElementArray[serialCommand.ElementNumber].ActuatorID);
      Serial.print(":");
      Serial.println(ActuatorElementArray[serialCommand.ElementNumber].ActuatorVal);
    }
    else if (serialCommand.CommandChar == 'W') {
         //Write (set) the actuator to proper value
      ActuatorElementArray[serialCommand.ElementNumber].ActuatorVal = serialCommand.CommandInt;
      analogWrite(ActuatorElementArray[serialCommand.ElementNumber].ActuatorPin, serialCommand.CommandInt);  //turn on the actuator to proper %
      //************Have to add in an IF here for AC outlets***************************
      success();
    }
    else if (serialCommand.CommandChar == 'I') {
        //Change the Elements ID
      EEPROM.updateBlock<char>(ActuatorElementArray[serialCommand.ElementNumber].ActuatorEEPROMIDLocation, serialCommand.CommandValCharArray, 8);
      EEPROM.readBlock<char>(ActuatorElementArray[serialCommand.ElementNumber].ActuatorEEPROMIDLocation, ActuatorElementArray[serialCommand.ElementNumber].ActuatorID, 8);
      success();
    }
    else if (serialCommand.CommandChar == 'S') {
        //Set the Fail Safe Value
      EEPROM.updateInt(ActuatorElementArray[serialCommand.ElementNumber].ActuatorEEPROMFSLocation, serialCommand.CommandInt);  //Save value to EEPROM);
      ActuatorElementArray[serialCommand.ElementNumber].ActuatorFSVal = serialCommand.CommandInt;
      success();
    }
    else if (serialCommand.CommandChar == 'F') {
        //Read the Fail Safe Value
      Serial.print(GroupID);
      Serial.print("/");
      Serial.print(ActuatorElementArray[serialCommand.ElementNumber].ActuatorID);
      Serial.print(":");
      Serial.println(EEPROM.readInt(ActuatorElementArray[serialCommand.ElementNumber].ActuatorEEPROMFSLocation));
    }
    else {
      fail();  //Return a -1
    }
  clearTheString();
  }
  
  else {
      //Measure pH1, pH2, Salinity, PCB Temp, DIY Pins  once a second, put them in an array
    TimeNow = millis();  //Check the time
    if (TimeNow > LastMeasurement + 1000) {
        LastMeasurement = millis();
        
          
      for (int i = 0; i < numSensors; i++) {
           // subtract the last reading:
        SensorElementArray[i].SensorTotal = SensorElementArray[i].SensorTotal - SensorElementArray[i].SensorVal[index];
           // read from the sensor:
        SensorElementArray[i].SensorVal[index] = analogRead(SensorElementArray[i].SensorPin);
           // add the reading to the total:
        SensorElementArray[i].SensorTotal = SensorElementArray[i].SensorTotal + SensorElementArray[i].SensorVal[index];
           // calculate the average:     
        SensorElementArray[i].SensorAvg = SensorElementArray[i].SensorTotal / numReadings; 
           //Calibrate
        SensorElementArray[i].SensorCalVal = SensorElementArray[i].SensorAvg + SensorElementArray[i].SensorCalBVal;
        SensorElementArray[i].SensorCalVal = SensorElementArray[i].SensorCalVal / SensorElementArray[i].SensorCalMVal;
      }
          // advance to the next position in the array:  
      index = index + 1;                    
          // if we're at the end of the array...
      if (index >= numReadings)          {    
        // ...wrap around to the beginning:
        index = 0;      
      } 
    }  
  }
}


//Deals with incoming Serial Messages
void serialEvent() {
  String GroupIDIn = "";
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    InputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is an "!" check out the string
      for (int i = 0; i<= 7; i++) {  //Read the ID
        inChar = InputString.charAt(i);
          GroupIDIn += inChar;
      }
      if (GroupIDIn == GroupID) {   //If the ID from serial matches the ID from eeprom, accept the command
        WatchDogTime = millis();  //Check the time
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
            //If it is a sensor command
        if (InputString.charAt(8) == 'e')    { 
          serialCommand.Sensor = true;    //Tell the main program there is input
           //Grab out the element ID, command, and value 
          String serialReadString = "";
                        //Load the Element ID
          for (int i = 8; i < 16; i++) {  
            char ElementCommandInChar = InputString.charAt(i);
            serialReadString += ElementCommandInChar;
          }
              //convert to Char array for comparison
          serialReadString.toCharArray(serialCommand.CommandValCharArray, (serialReadString.length()+1));  //+1 to make it work
          serialReadString = ""; 
             //Compare it to the string of stored element id's, and say which # it is
             boolean foundElement = true;
          for (int i = 0; i < numSensors; i++) {
            if (strcmp(SensorElementArray[i].SensorID, serialCommand.CommandValCharArray) == 0) {  //strcmp compares the two char arrays & returns a 0 if they are the same
              serialCommand.ElementNumber = i;
              foundElement = false;
            }
            if ((i == (numSensors - 1)) && (foundElement)) {
            fail();
            clearTheString(); 
            }
          }
              //Clear out the string
          for(int i = 0; i < 8; i++) {
            serialCommand.CommandValCharArray[i] = ' ';
          } 
              //Load the Element Command Character  
          serialCommand.CommandChar = InputString.charAt(16);
              //Load the Element Command Value
          for (int i = 18; i < 26; i++) {  
            char ElementCommandInChar = InputString.charAt(i);
            serialReadString += ElementCommandInChar;
          }
          serialReadString.toCharArray(serialCommand.CommandValCharArray, (serialReadString.length()+1));   //+1 to make it work
          serialCommand.CommandValFloat = serialReadString.toFloat();
          serialReadString = ""; 
             

        }
              //If it is an actuator command
        else if (InputString.charAt(8) == 'a') {
          serialCommand.Actuator = true;    //Tell the main program there is input
          //Grab out the element ID, command, and value 
          String serialReadString = "";
              //Load the Element ID
          for (int i = 8; i < 16; i++) {  
            char ElementCommandInChar = InputString.charAt(i);
            serialReadString += ElementCommandInChar;
          }
               //convert to Char array for comparison
          serialReadString.toCharArray(serialCommand.CommandValCharArray, (serialReadString.length()+1));  //+1 to make it work
          serialReadString = ""; 
          //Compare it to the char array of stored element id's, and say which # it is
          boolean foundElement = true;
          for (int i = 0; i < numActuators; i++) {
            if (strcmp(ActuatorElementArray[i].ActuatorID, serialCommand.CommandValCharArray) == 0) {  //strcmp compares the two char arrays & returns a 0 if they are the same
              serialCommand.ElementNumber = i;
              foundElement = false;
            }
            if ((i == (numSensors - 1)) && (foundElement)) {
            fail();
            clearTheString(); 
            }
          }
            //Clear out the string
          for(int i = 0; i < 8; i++) {
            serialCommand.CommandValCharArray[i] = ' ';
          } 
              //Load the Element Command Character  
          serialCommand.CommandChar = InputString.charAt(16);
              //Load the Element Command
          for (int i = 18; i < 26; i++) {  
            char ElementCommandInChar = InputString.charAt(i);
            serialReadString += ElementCommandInChar;
          }
          serialReadString.toCharArray(serialCommand.CommandValCharArray, (serialReadString.length()+1));   //+1 to make it work
          serialCommand.CommandInt = serialReadString.toInt();   
          serialReadString = "";    
        }
            //It's a Group command, hopefully
        else { 
          serialCommand.Group = true;
          String serialReadString = "";
              //Sets the Case statement Switch
          serialCommand.CommandInt = InputString.charAt(8) - '0';  //converting char to int
          int Comand1 = InputString.charAt(9) - '0';  //converting char 2 to int
          if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
            serialCommand.CommandInt = serialCommand.CommandInt * 10;
            serialCommand.CommandInt += Comand1;  
          } 
          int StringLength = InputString.length();
          if (((InputString.charAt(9) - '0') >= 0) && ((InputString.charAt(9) - '0') <=9)) {  //If it is a 2 digit request
            for (int i = 11; i < StringLength; i++) {
              char ActuatorRead = InputString.charAt(i);
              serialReadString += ActuatorRead;
            }
          }
          else {   //If it is a 1 digit request
            for (int i = 10; i < StringLength; i++) {
              char ActuatorRead = InputString.charAt(i);
              serialReadString += ActuatorRead;
            }
          }
          serialReadString.toCharArray(serialCommand.CommandValCharArray, serialReadString.length());    //convert to Char array
          serialCommand.CommandValInt = serialReadString.toInt();  //An integer of the serial input
        }
      } 
      else if ((InputString.charAt(0) == '0') && (InputString.length() == 2)) {
        //Serial.print("ID is: ");
        Serial.print(GroupID);  //Print out ID
        Serial.print("/");
        Serial.println(GroupID);
        serialCommand.Group = false;
        InputString = "";
      }
      else {
        fail();  //Return a -1
        clearTheString();
      }
      GroupIDIn = "";
    
    
    }  
  }  
}


//Send odroid a -1 if it does't like the request
void fail() {
  Serial.print(GroupID);  //Print out ID
  Serial.print("/");
  Serial.println("-1");
}


//send odoid a 1 if everything worked out well
void success() {
  Serial.print(GroupID);  //Print out ID
  Serial.print("/");
  Serial.println("1");
}


//Clear the string
void clearTheString() {
  InputString = "";
  serialCommand.Sensor = false;
  serialCommand.Actuator = false;
  serialCommand.Group = false;
  serialCommand.CommandInt = 0;
  serialCommand.CommandValInt = 0;
  serialCommand.CommandChar = ' ';
  serialCommand.CommandValFloat = 0;
  serialCommand.ElementNumber = 0;
  for(int i = 0; i < 8; i++) {
    serialCommand.CommandValCharArray[i] = ' ';
  }   
}

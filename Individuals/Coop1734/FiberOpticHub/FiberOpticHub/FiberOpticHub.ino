/*  Reef POM Fiber Optic Hub v6 (GPIO version)
 * 
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
Pin   Function
---------------
2     LED
3     IR-reciever 1kohm to 5v
4     IR-reciever 500ohm to 5v
5     DIY #4
6     DIY #3
7     IR-reference 15k to 5v
8     IR-reference 3k to 5v
9     IR-reference 43k to 5v
10    IR-Send pin
11    DIY #1
12    DIY #2
13    Leak Sensor 3.5mm Jack
14    IR-Recieve pin
15    DIY #5
16    DIY #8
17    DIY #7
18    DIY #6

Fiber Optic Hub v6 Element List (GPIO Version)
Element, Function, Pin#
1  ,  Leak Sense  , 13 
2  ,  DIY#1       , 11 
3  ,  DIY#2       , 12 Power for Moisture Sensors
4  ,  DIY#3       , 6  door
5  ,  DIY#4       , 5  temp
6  ,  DIY#5       , 15 
7  ,  DIY#6       , 18 moisture 1
8  ,  DIY#7       , 17 mositure 2
9  ,  DIY#8       , 16 motion
*/

#include <OspomLite.h>  //include the ospom library
#include <EEPROMex.h>  //This must be included for Ospom to work
#include <OneWire.h>   // DS18S20 Temperature chip i/o
//#include <CapacitiveSensor.h>
OspomLite ospomLite;  // instantiate ospom, an instance of Ospom Library  Don't put () if it isn't getting a variable
//**If doing Triac Dimming, set phase pin in Ospom.cpp at line 682**  ToDo: make this setable here & internet

OneWire ds(5);  // <--- Set Pin Here   & set element number lower down.
unsigned long previousMillis = 0;

void setup() {  // put your setup code here, to run once:
  ospomLite.Setup();  //ospom.Setup initilizes ospom 
}

void loop() {
  ospomLite.Run(1000);  //Activates the ospom library every time the loop runs **This is required
        //Delay in milliseconds between reads (333 = Read Sensors 3 times a second, 1000 = 1 time a second.)
    /*
    Put any standard arduino code in here that you want to run over and over again.
    OSPOM Functions:
    ospom.define(Pin Number, Pin Type, PinFunction, PinID);  = Define OSPOM Internet Dashboard accessable Sensor or Actuator Elements
               example: ospom.define(7, 's', ospom.anaRead, "stfw0000");
    ospom.read();  = Read OSPOM Sensors or Actuators and recieve the result
    ospom.write();  = Write OSPOM Sensors or Actuators using this function
       **Writing a pin that is part of the OSPOM dashboard without using this function will cause
           the dashboard to be confused
    ospom.Set(element Number(int)), value(float));  = Send your sensor reading to an ospom element so it is displayed on the dashboard
    
    **Dont forget to Write your own OSPOM Configuation Sketch to setup your arduino, 
       or run the OSPOM Generic Configuration sketch at least once before calling any ospom functions
    */

/*
  //Read the SunDrops and Leak Sensor in here, then send the data to the ospom elements for web access
  unsigned long Duration;
  Duration = pulseIn(13, LOW);  //reads length of a pulse in microseconds
  if (Duration != 0) {
    ospomLite.Set(1, float(Duration));  //Sets ospom Element #1 (Leak Sensor) to the amount of water measured
  }                                 //Higher = more wet
*/

  //One Wire Temperature Probe reading section.  This can be refactored.
  int HighByte, LowByte, TReading;
  float Tc_100;
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  ds.reset_search();
  if ( !ds.search(addr)) {
//      Serial.print("No more addresses.\n");
//      ds.reset_search();
//      return;
  }
  ds.reset();
  ds.select(addr);      //SET TO 12 BIT AROUND HERE 
  ds.write(0x44,1);         // start conversion, with parasite power on at the end
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= 1000) {
    // save the last time
    previousMillis = currentMillis;   
    present = ds.reset();
    ds.select(addr);    
    ds.write(0xBE);         // Read Scratchpad

    for ( i = 0; i < 9; i++) {           // we need 9 bytes
      data[i] = ds.read();
    }

    LowByte = data[0];
    HighByte = data[1];
    TReading = (HighByte << 8) + LowByte;
    Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25
    float Tc = Tc_100 / 100;
    float Tf = Tc * 1.8; 
    Tf = Tf + 32;
    ospomLite.Set(5, Tf);  //  <-Set element number here

    //Read Soil Moisture Sensors
    digitalWrite(12, HIGH);
    delay(10);
    float sm1 = analogRead(18);
    float sm2 = analogRead(17);
    float mo = digitalRead(16);
    ospomLite.Set(7, sm1);  //  <-Set element number here 
    ospomLite.Set(8, sm2);  //  <-Set element number here
    ospomLite.Set(9, mo);    
    digitalWrite(12, LOW);
  }
}


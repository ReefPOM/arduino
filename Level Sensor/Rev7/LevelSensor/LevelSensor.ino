/* Level Sensor Rev7&8 Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

Arduino Pinout: 
2 = capsense fill 1
3 = capsense read 1
4 = capsense read 2
5 = capsense read 6
6 = capsense read 7
7 = capsense read 8
8 = capsense read 9
9 = capsense read 10
10 = capsense read 5
11 = capsense read 4
12 = capsense read 11
13 = capsense read 3
14 = Flow senser power
15 = Ambient Light Sensor
16 = Temp read
17 = Flow read
21 = 3.3v reference
*/

#include <CapacitiveSensor.h>    //Capacitive Sensing Library
#include <Wire.h>       //I2C Library
#include "TLC59108.h"   //LED Controller Library
#include <EEPROMex.h>  //For Calibration Data and ID

#define HW_RESET_PIN 2   //Stuff for I2C LED drivers
#define I2C_ADDR TLC59108::I2C_ADDR::BASE
#define I2C_ADDR2 TLC59108::I2C_ADDR2::BASE2  //2nd led driver
TLC59108 leds(I2C_ADDR);
TLC59108 leds2(I2C_ADDR2);  //2nd led driver

//Variables for testing
int LastWetPad = 0;
int PadChange = 0;
boolean SkipOne = false;
 
 
//Variable needed by all Ai Arduino's:
String InputString = "";
boolean StringComplete = false;
boolean Pause = false;
int Comand = 0;
int Comand1 = 0;
unsigned long WatchDogTime = 0;
int WatchDogEnable = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
unsigned long LastMeasurement = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
char ID[] = "        ";
int ActuatorValInt = 0;   //Processed String Data in 3 data types:
float ActuatorValFloat = 0;
String ActuatorValString = "";
char ActuatorValCharArray[] = "        ";



//Analog Reading Section
const int numReadings = 30;  //number of analog readings in array
int index = 0;        // the index of the current reading
int Temp[numReadings];  // the readings from the analog input as an array
float TempTotal = 0;    // the running total
float TempAvg = 0;    //The average
float CalTempc = 0;  //Calibrated
float CalTempf = 0;
char TempfID[] = "        ";
char TempcID[] = "        ";
int Flow[numReadings];
int FlowTotal = 0;
int FlowAvg = 0;
int CalFlow = 0;
char FlowID[] = "        ";
int Ambient[numReadings];
int AmbientTotal = 0;
int AmbientAvg = 0;
char AmbientID[] = "        ";
//int CalAmbient = 0;
int Voltage[numReadings];      
int VoltageTotal = 0;                 
int VoltageAvg = 0;
float CalVoltage = 0;
char VoltageID[] = "        ";
int levelval = 0;
char LevelID[] = "        ";
int LEDB = 0;
char LEDBID[] = "        ";
char DimFactorID[] = "        ";
int DimFactor = 0;
char AmbCntrlID[] = "        ";
int LEDambCntrl = 0;
byte LEDHEXbyte;

float FlowCalM = 0;  //calibration variables
float FlowCalB = 0;
float TempCalM = 0;
float TempCalB = 0;

const int FlowPowerPin = 14;  //turns on pwr to flow heater
const int TempPin = 16;
const int FlowReadPin = 17;
const int AmbientPin = 15;
const int VoltagePin =21;

const int CapsensefillPin1 = 2;
const int CapsenseReadPin1 = 3;
const int CapsenseReadPin2 = 4;
const int CapsenseReadPin6 = 5;
const int CapsenseReadPin7 = 6;
const int CapsenseReadPin8 = 7;
const int CapsenseReadPin9 = 8;
const int CapsenseReadPin10 = 9;
const int CapsenseReadPin5 = 10;
const int CapsenseReadPin4 = 11;
const int CapsenseReadPin11 = 12;
const int CapsenseReadPin3 = 13;

//Capacitive sensor
int CapVal11;
int CapVal10;
int CapVal9;
int CapVal8;
int CapVal7;
int CapVal6;
int CapVal5;
int CapVal4;
int CapVal3;
int CapVal2;
int CapVal1;

 //Definng which pins are the (Fill,Sense) pins.
CapacitiveSensor   LevelSensor11 = CapacitiveSensor(2,12);
CapacitiveSensor   LevelSensor10 = CapacitiveSensor(2,9);
CapacitiveSensor   LevelSensor9 = CapacitiveSensor(2,8);
CapacitiveSensor   LevelSensor8 = CapacitiveSensor(2,7);
CapacitiveSensor   LevelSensor7 = CapacitiveSensor(2,6);
CapacitiveSensor   LevelSensor6 = CapacitiveSensor(2,5);
CapacitiveSensor   LevelSensor5 = CapacitiveSensor(2,10);
CapacitiveSensor   LevelSensor4 = CapacitiveSensor(2,11);
CapacitiveSensor   LevelSensor3 = CapacitiveSensor(2,13);
CapacitiveSensor   LevelSensor2 = CapacitiveSensor(2,4);
CapacitiveSensor   LevelSensor1 = CapacitiveSensor(2,3);


//level measuring variables
int cap11max = 1150;
int cap11min = 0;
int cap10max = 1071;
int cap10min = 0;
int cap9max = 1097;
int cap9min = 0;
int cap8max = 1391;
int cap8min = 0;
int cap7max = 1374;
int cap7min = 0;
int cap6max = 1210;
int cap6min = 0;
int cap5max = 1192;
int cap5min = 0;
int cap4max = 1279;
int cap4min = 0;
int cap3max = 1401;
int cap3min = 0;
int cap2max = 1638;
int cap2min = 0;
int cap1max = 1963;
int cap1min = 0;
 
void setup() {
  Serial.begin(57600);
 // reserve 200 bytes for the InputString:
  InputString.reserve(200);
 
   // turn off callibration
  LevelSensor11.set_CS_Timeout_Millis(4000);
  LevelSensor11.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor10.set_CS_Timeout_Millis(4000);
  LevelSensor10.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor9.set_CS_Timeout_Millis(4000);
  LevelSensor9.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor8.set_CS_Timeout_Millis(4000);
  LevelSensor8.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor7.set_CS_Timeout_Millis(4000);
  LevelSensor7.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor6.set_CS_Timeout_Millis(4000);
  LevelSensor6.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor5.set_CS_Timeout_Millis(4000);
  LevelSensor5.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor4.set_CS_Timeout_Millis(4000);
  LevelSensor4.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor3.set_CS_Timeout_Millis(4000);
  LevelSensor3.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor2.set_CS_Timeout_Millis(4000);
  LevelSensor2.set_CS_AutocaL_Millis(0xFFFFFFFF);
  LevelSensor1.set_CS_Timeout_Millis(4000);
  LevelSensor1.set_CS_AutocaL_Millis(0xFFFFFFFF);

  //Setup I2C for LED's
  Wire.begin();
  leds.init(HW_RESET_PIN);  //operates 1st led driver
  leds.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);
  leds2.init(HW_RESET_PIN);  //operates 2nd led driver
  leds2.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);

  // initialize all the analog readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    Temp[thisReading] = 0;  
    Flow[thisReading] = 0;  
    Ambient[thisReading] = 0;  
    Voltage[thisReading] = 0;
  }
  
  pinMode(FlowPowerPin, OUTPUT);
  pinMode(AmbientPin, INPUT);
  pinMode(VoltagePin, INPUT);
  
  for(byte i = 0; i < 0xff; i++) {  //dim on the leds
    leds.setAllBrightness(i);
    leds2.setAllBrightness(i);
    delay(7);
  }
  for(byte i = 0xff; i > 0; i--) {  //dim off the leds to 1%
    leds.setAllBrightness(i);
    leds2.setAllBrightness(i);
    delay(4);
  }
  
  WatchDogTime = millis();  //Delay the watchdog
  
EEPROM.readBlock<char>(0, ID, 8);
EEPROM.readBlock<char>(8, FlowID, 8);
EEPROM.readBlock<char>(24, TempfID, 8);
EEPROM.readBlock<char>(40, TempcID, 8);
EEPROM.readBlock<char>(56, LevelID, 8);
EEPROM.readBlock<char>(72, AmbientID, 8);
EEPROM.readBlock<char>(88, VoltageID, 8);
EEPROM.readBlock<char>(104, LEDBID, 8);
EEPROM.readBlock<char>(112, AmbCntrlID, 8);
EEPROM.readBlock<char>(120, DimFactorID, 8);
LEDB = EEPROM.readInt(128);  //Read LED brightness
LEDambCntrl = EEPROM.readInt(130);  //Read if LED's are Ambient Controlled
DimFactor = EEPROM.readInt(132);  //Read LED dimming factor
WatchDogEnable = EEPROM.readInt(180);  //Watchdog Enable/Diable
FlowCalM = EEPROM.readFloat(16);
FlowCalB = EEPROM.readFloat(20);
TempCalM = EEPROM.readFloat(32);
TempCalB = EEPROM.readFloat(36);

cap1min = EEPROM.readInt(178);  //Save value to EEPROM
cap2min = EEPROM.readInt(174);  //Save value to EEPROM
cap3min = EEPROM.readInt(170);  //Save value to EEPROM
cap4min = EEPROM.readInt(166);  //Save value to EEPROM
cap5min = EEPROM.readInt(162);  //Save value to EEPROM
cap6min = EEPROM.readInt(158);  //Save value to EEPROM
cap7min = EEPROM.readInt(154);  //Save value to EEPROM
cap8min = EEPROM.readInt(150);  //Save value to EEPROM
cap9min = EEPROM.readInt(146);  //Save value to EEPROM
cap10min = EEPROM.readInt(142);  //Save value to EEPROM
cap11min = EEPROM.readInt(138);  //Save value to EEPROM
/*
cap1max = EEPROM.readInt(160);  //Save value to EEPROM
cap2max = EEPROM.readInt(156);  //Save value to EEPROM
cap3max = EEPROM.readInt(152);  //Save value to EEPROM
cap4max = EEPROM.readInt(148);  //Save value to EEPROM
cap5max = EEPROM.readInt(144);  //Save value to EEPROM
cap6max = EEPROM.readInt(140);  //Save value to EEPROM
cap7max = EEPROM.readInt(136);  //Save value to EEPROM
cap8max = EEPROM.readInt(132);  //Save value to EEPROM
cap9max = EEPROM.readInt(128);  //Save value to EEPROM
cap10max = EEPROM.readInt(124);  //Save value to EEPROM
cap11max = EEPROM.readInt(120);  //Save value to EEPROM
*/
//Assign a group ID of 00000000 if it doesn't have one
      char a = EEPROM.read(0);
      //Serial.println(a);
      if (a != 'g') {
        EEPROM.updateBlock<char>(0,"00000000", 8);
    //    Serial.println("writing 0's to EEPROM");
        EEPROM.readBlock<char>(0, ID, 8);
        Serial.println(ID);
       }
//Ask for Actuator Data at Startup
  Serial.print(ID);
  Serial.print("/");
  Serial.println("SendActData");
}
// the loop routine runs over and over again forever:
void loop() {
    //delay(100); 
  
  TimeNow = millis();  //Check the time
  //Serial.println("Running Loop");
  
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(ID);  //Group ID
      Serial.print("/");
      Serial.print(FlowID);  
      Serial.print(":");
      Serial.print(FlowAvg);    //Flow
      Serial.print(",");
      Serial.print(TempfID);  
      Serial.print(":");    
      Serial.print(CalTempf);  //Temp in Farenheight
      Serial.print(",");
      Serial.print(TempcID);  //Temp in Celcius
      Serial.print(":");
      Serial.print(CalTempc);
      Serial.print(",");
      Serial.print(LevelID);  //Water level in 0-100%
      Serial.print(":");
      Serial.print(levelval);  //STOP SENDING HERE for 6th ORDER
      Serial.print(",");
      Serial.print(AmbientID);    //Ambient Light Level
      Serial.print(":");
      Serial.print(AmbientAvg);
      Serial.print(",");
      Serial.print(VoltageID);    //Voltage of Board
      Serial.print(":");
      Serial.println(CalVoltage);
      LastSend = millis();
    }
  }
  
  if (WatchDogEnable == 1) {
    if (TimeNow > WatchDogTime + 300000) {  //If the arduino hasn't recieved any signal in 5 minutes
        //Initiate Fail-States
      for(byte q = 0; q < 0xff; q++) {  //dim on the leds
        leds.setAllBrightness(q);
        leds2.setAllBrightness(q);
        delay(5);
      }
      for(byte q = 0xff; q > 0; q--) {  //dim off the leds to 1%
        leds.setAllBrightness(q);
        leds2.setAllBrightness(q);
        delay(5);
      }
    }  
  }  
  
//Decides what to do when it recieves a serial command
  if (StringComplete) {
    WatchDogTime = millis();  //Check the time
    Comand = InputString.charAt(8) - '0';  //converting char to int
    Comand1 = InputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = Comand * 10;
      Comand += Comand1;
    }
    switch (Comand) {
      case 0:
        //Read ID
        EEPROM.readBlock<char>(0, ID, 8);
        Serial.print(ID);
        Serial.print("/");
        Serial.println(ID);
        
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 

 /*      case 1:
         //Send Back Sensor Data
        Serial.print(ID);  //Level Sensor
        Serial.print("/");  
        Serial.print("Flow,");
        Serial.print(FlowAvg);
        Serial.print(",");    
        Serial.print("Temp,");
        Serial.print(TempAvg);
        Serial.print(",");
        Serial.print("Ambient,");
        Serial.print(AmbientAvg);
        Serial.print(",");
        Serial.print("vRef,");
        Serial.print(VoltageAvg);
        Serial.print(",");
        Serial.print("C11,");
        Serial.print(CapVal11);
        Serial.print(",");
        Serial.print("C10,");
        Serial.print(CapVal10);
        Serial.print(",");
        Serial.print("C9,");
        Serial.print(CapVal9);
        Serial.print(",");
        Serial.print("C8,");
        Serial.print(CapVal8);
        Serial.print(",");
        Serial.print("C7,");
        Serial.print(CapVal7);
        Serial.print(",");
        Serial.print("C6,");
        Serial.print(CapVal6);
        Serial.print(",");
        Serial.print("C5,");
        Serial.print(CapVal5);
        Serial.print(",");
        Serial.print("C4,");
        Serial.print(CapVal4);
        Serial.print(",");
        Serial.print("C3,");
        Serial.print(CapVal3);
        Serial.print(",");
        Serial.print("C2,");
        Serial.print(CapVal2);
        Serial.print(",");
        Serial.print("C1,");
        Serial.println(CapVal1);    
        // clear the string:
        InputString = "";
        StringComplete = false;
        break;
*/      
/*      case 2:
          //Turn ON flow heater power
        digitalWrite(FlowPowerPin, HIGH);
  //    Serial.println("Flow Heater ON");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
        
        
      case 3:
          //Turn OFF flow heater power
        digitalWrite(FlowPowerPin, LOW);
    //    Serial.println("Flow Heater OFF");
                          // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
        
      case 4:
          //Turn on LEDs to set brightness
        calcfunction();  //Grab the dimmed value
        ActuatorValInt = ActuatorValInt * 255 / 100;  //convert to 0-255 for analog write
        LEDHEXbyte = (byte)ActuatorValInt;      //Convert to a byte for the LED driver
        leds.setAllBrightness(LEDHEXbyte);  //Set all the LED's brightness
                  // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
        
        case 5:
          //Turn on a certain channel for testing
          calcfunction();  //Grab the dimmed value
          int channel; 
          channel = ActuatorValInt;
          
          leds.setBrightness(channel, 0xff);
*/           
        case 6:  //Run the Level Sensing Function with printout
          getLevel(true);
          //Clear the string:
          InputString = "";
          StringComplete = false;
          Comand = 0;
          Comand1 = 0;
          ActuatorValString = "";
          ActuatorValInt = 0;
          break;   
        
      case 10: 
         //Send Back Sensor Data to Ai
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.print(FlowID);  
        Serial.print(":");
        Serial.print(FlowAvg);    //Flow
        Serial.print(",");
        Serial.print(TempfID);  
        Serial.print(":");
        Serial.print(CalTempf);  //Temp in Farenheight
        Serial.print(",");
        Serial.print(TempcID);  //Temp in Celcius
        Serial.print(":");
        Serial.print(CalTempc);
        Serial.print(",");
        Serial.print(LevelID);  //Water level in 0-100%
        Serial.print(":");
        Serial.print(levelval);  //STOP SENDING HERE for 6th ORDER
        Serial.print(",");
        Serial.print(AmbientID);    //Ambient Light Level
        Serial.print(":");
        Serial.print(AmbientAvg);
        Serial.print(",");
        Serial.print(VoltageID);    //Voltage of Board
        Serial.print(":");
        Serial.println(CalVoltage);
                  // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
        
      case 11:
          //Send Back Data for 60 Seconds
        calcfunction();  //Grab the time in milliseconds
        SendDelay = ActuatorValInt;
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
                  // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      case 13:
          //Send Back Actuator Data
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.print(LEDBID);  
        Serial.print(":");
        Serial.print(LEDB);
        Serial.print(",");
        Serial.print(AmbCntrlID);  
        Serial.print(":");
        Serial.print(LEDambCntrl);
        Serial.print(",");  
        Serial.print(DimFactorID);
        Serial.print(":");
        Serial.println(DimFactor);
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;   
         
      case 14: 
          //Write LED Brightness Value (0-100%)
        calcfunction();
          EEPROM.updateInt(128, ActuatorValInt);  //Save value to EEPROM
          LEDB = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
          // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;   
        
      case 15:
        //Set LEDs as controlled or not controlled by ambient
        calcfunction();
        EEPROM.updateInt(130, ActuatorValInt);  //Save value to EEPROM
        LEDambCntrl = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;   
        
      case 16:  
      //Set LED dimming factor  
        calcfunction();
        EEPROM.updateInt(132, ActuatorValInt);  //Save value to EEPROM
        DimFactor = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
           // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;   
        
      case 17:
        //Enable/Disable Watchdog timer (1/0)
        calcfunction();
        EEPROM.updateInt(180, ActuatorValInt);  //Save value to EEPROM
        WatchDogEnable = ActuatorValInt;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      case 18:
        //Write Level Sensor Mins to EEPROM
        //LEVEL SENSOR MUST BE OUT OF WATER
        EEPROM.updateInt(178, CapVal1);  //Save value to EEPROM
        EEPROM.updateInt(174, CapVal2);  //Save value to EEPROM
        EEPROM.updateInt(170, CapVal3);  //Save value to EEPROM
        EEPROM.updateInt(166, CapVal4);  //Save value to EEPROM
        EEPROM.updateInt(162, CapVal5);  //Save value to EEPROM
        EEPROM.updateInt(158, CapVal6);  //Save value to EEPROM
        EEPROM.updateInt(154, CapVal7);  //Save value to EEPROM
        EEPROM.updateInt(150, CapVal8);  //Save value to EEPROM
        EEPROM.updateInt(146, CapVal9);  //Save value to EEPROM
        EEPROM.updateInt(142, CapVal10);  //Save value to EEPROM
        EEPROM.updateInt(138, CapVal11);  //Save value to EEPROM
        
        cap1min = EEPROM.readInt(178);  //Save value to EEPROM
        cap2min = EEPROM.readInt(174);  //Save value to EEPROM
        cap3min = EEPROM.readInt(170);  //Save value to EEPROM
        cap4min = EEPROM.readInt(166);  //Save value to EEPROM
        cap5min = EEPROM.readInt(162);  //Save value to EEPROM
        cap6min = EEPROM.readInt(158);  //Save value to EEPROM
        cap7min = EEPROM.readInt(154);  //Save value to EEPROM
        cap8min = EEPROM.readInt(150);  //Save value to EEPROM
        cap9min = EEPROM.readInt(146);  //Save value to EEPROM
        cap10min = EEPROM.readInt(142);  //Save value to EEPROM
        cap11min = EEPROM.readInt(138);  //Save value to EEPROM

        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;  
        
      case 30:
          //Write Group ID to EEPROM
        calcfunction();
        EEPROM.updateBlock<char>(0, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(0, ID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
                 // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
       
       
      case 31:  
        //Write 1st Element ID to EEPROM *Flow*
        calcfunction();
        EEPROM.updateBlock<char>(8, ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(8, FlowID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      case 32:  
        //Write 2nd Element ID to EEPROM *Temp F*
        calcfunction();
        EEPROM.updateBlock<char>(24,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(24, TempfID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
      // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;  
       
      case 33:  
        //Write 3rd Element ID to EEPROM *Temp C* 
        calcfunction();
        EEPROM.updateBlock<char>(40,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(40, TempcID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      case 34:  
        //Write 4th Element ID to EEPROM *Level*  
        calcfunction();
        EEPROM.updateBlock<char>(56,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(56, LevelID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
       
      case 35:  
        //Write 5th Element ID to EEPROM *Ambient Light Level*
        calcfunction();
        EEPROM.updateBlock<char>(72,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(72, AmbientID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
       
      case 36:  
        //Write 6th Element ID to EEPROM *Voltage*
        calcfunction();
        EEPROM.updateBlock<char>(88,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(88, VoltageID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
       
      case 37:  
        //Write 7th Element ID to EEPROM *LED Brightness* 
        calcfunction();
        EEPROM.updateBlock<char>(104,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(104, LEDBID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;  
        
      case 38:  
        //Write 7th Element ID to EEPROM *Ambient Control Of LED's Brightness* 
        calcfunction();
        EEPROM.updateBlock<char>(112,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(112, AmbCntrlID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;  
        
      case 39:  
        //Write 7th Element ID to EEPROM *Dimming Factor for ambient impact on LED brightess (2isgood)* 
        calcfunction();
        EEPROM.updateBlock<char>(120,ActuatorValCharArray, 8);
        //Read ID
        EEPROM.readBlock<char>(120, DimFactorID, 8);
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;  
        
/*      case 39:
        //Write Level Sensor Maxs to EEPROM
        //LEVEL SENSOR MUST BE SUBMERGED To the top magnet
        EEPROM.writeInt(160, CapVal1);  //Save value to EEPROM
        EEPROM.writeInt(156, CapVal2);  //Save value to EEPROM
        EEPROM.writeInt(152, CapVal3);  //Save value to EEPROM
        EEPROM.writeInt(148, CapVal4);  //Save value to EEPROM
        EEPROM.writeInt(144, CapVal5);  //Save value to EEPROM
        EEPROM.writeInt(140, CapVal6);  //Save value to EEPROM
        EEPROM.writeInt(136, CapVal7);  //Save value to EEPROM
        EEPROM.writeInt(132, CapVal8);  //Save value to EEPROM
        EEPROM.writeInt(128, CapVal9);  //Save value to EEPROM
        EEPROM.writeInt(124, CapVal10);  //Save value to EEPROM
        EEPROM.writeInt(120, CapVal11);  //Save value to EEPROM
        
        cap1max = EEPROM.readInt(160);  //Save value to EEPROM
        cap2max = EEPROM.readInt(156);  //Save value to EEPROM
        cap3max = EEPROM.readInt(152);  //Save value to EEPROM
        cap4max = EEPROM.readInt(148);  //Save value to EEPROM
        cap5max = EEPROM.readInt(144);  //Save value to EEPROM
        cap6max = EEPROM.readInt(140);  //Save value to EEPROM
        cap7max = EEPROM.readInt(136);  //Save value to EEPROM
        cap8max = EEPROM.readInt(132);  //Save value to EEPROM
        cap9max = EEPROM.readInt(128);  //Save value to EEPROM
        cap10max = EEPROM.readInt(124);  //Save value to EEPROM
        cap11max = EEPROM.readInt(120);  //Save value to EEPROM

        Serial.println("1");
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
*/      
      case 40:
        //Write 1st Element Calibration Slope (M) to EEPROM *Flow* eep-16
        calcfunction();
        EEPROM.updateFloat(16, ActuatorValFloat);  //Save value to EEPROM
        FlowCalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      case 41:
        //Write 1st Element Calibration Y-inter (B) to EEPROM *Flow* ee-20
        calcfunction();
        EEPROM.updateFloat(20, ActuatorValFloat);  //Save value to EEPROM
        FlowCalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
        
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
      
      case 42:
        //Write 2nd Element Calibration Slope (M) to EEPROM *Temp* ee-32
        calcfunction();
        EEPROM.updateFloat(32, ActuatorValFloat);  //Save value to EEPROM
        TempCalM = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
   /*     Serial.print("ActuatorValFloat: ");
        Serial.println(ActuatorValFloat);
        Serial.print("M from EEPROM: ");
        Serial.println(EEPROM.readFloat(32));
  */      
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
      
      case 43:
        //Write 2nd Element Calibration Y-inter (B) to EEPROM *Temp* ee-36
        calcfunction();
        EEPROM.updateFloat(36, ActuatorValFloat);  //Save value to EEPROM
        TempCalB = ActuatorValFloat;
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("1");
  /*      Serial.print("ActuatorValFloat: ");
        Serial.println(ActuatorValFloat);
        Serial.print("B from EEPROM: ");
        Serial.println(EEPROM.readFloat(36));
  */    
        // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break;
        
      default:
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("-1");
                // clear the string:
        InputString = "";
        StringComplete = false;
        Comand = 0;
        Comand1 = 0;
        ActuatorValString = "";
        ActuatorValInt = 0;
        break; 
        }
    }
    
    else {
//Adjust LED Brightness
     int LEDv = 0;
     if (LEDambCntrl == 1) {  //If LED's are ambient controlled
       LEDv = AmbientAvg / DimFactor;  //Divide by dimfactor (2?)
       LEDv = LEDv * LEDB / 100;
       LEDv = LEDv + 1;
       if (LEDv > 255) {  //LED driver only accepts values up to 255
          LEDv = 255;
       }
       else if (LEDB == 0) {  //Makes sure if the user wants the LED's off, they are off.
         LEDv = 0;
       }
     }
     else {
       LEDv = LEDB * 255 / 100;
     }
     LEDHEXbyte = (byte)LEDv;      //Convert to a byte for the LED driver
     
     
     if (TimeNow > LastMeasurement + 1000) {
        LastMeasurement = millis();
//Read Temp, Flow, CapSense, add to strings, average, calibrate
      // subtract the last reading:
      FlowTotal = FlowTotal - Flow[index];        
      TempTotal = TempTotal - Temp[index];
      AmbientTotal = AmbientTotal - Ambient[index];
      VoltageTotal = VoltageTotal - Voltage[index];
      
      // read from the sensor:  
      Flow[index] = analogRead(FlowReadPin);
      Temp[index] = analogRead(TempPin);
      Ambient[index] = analogRead(AmbientPin);
      Voltage[index] = analogRead(VoltagePin);
      //ground the wires above the capacitive pads
      digitalWrite(CapsenseReadPin1, LOW);
      digitalWrite(CapsenseReadPin2, LOW);
      digitalWrite(CapsenseReadPin3, LOW);
      digitalWrite(CapsenseReadPin4, LOW);
      digitalWrite(CapsenseReadPin5, LOW);
      digitalWrite(CapsenseReadPin6, LOW);
      digitalWrite(CapsenseReadPin7, LOW);
      digitalWrite(CapsenseReadPin8, LOW);
      digitalWrite(CapsenseReadPin9, LOW);
      digitalWrite(CapsenseReadPin10, LOW);
      digitalWrite(CapsenseReadPin11, LOW);
      CapVal11 = LevelSensor11.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin11, LOW);
      CapVal10 = LevelSensor10.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin10, LOW);
      CapVal9 = LevelSensor9.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin9, LOW);
      CapVal8 = LevelSensor8.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin8, LOW);
      CapVal7 = LevelSensor7.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin7, LOW);
      CapVal6 = LevelSensor6.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin6, LOW);
      CapVal5 = LevelSensor5.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin5, LOW);
      CapVal4 = LevelSensor4.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin4, LOW);
      CapVal3 = LevelSensor3.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin3, LOW);
      CapVal2 = LevelSensor2.capacitiveSensorRaw(4);
      digitalWrite(CapsenseReadPin2, LOW);
      CapVal1 = LevelSensor1.capacitiveSensorRaw(4);
      
      
      // add the reading to the total:
      FlowTotal= FlowTotal + Flow[index];
      TempTotal = TempTotal + Temp[index];
      AmbientTotal = AmbientTotal + Ambient[index];
      VoltageTotal = VoltageTotal + Voltage[index];
          
      // calculate the average:        
      FlowAvg = FlowTotal / numReadings;
      TempAvg = TempTotal / numReadings;
      AmbientAvg = AmbientTotal / numReadings;
      VoltageAvg = VoltageTotal / numReadings;
      
      //Calibrate
      CalVoltage = VoltageAvg - 1400;  //Calibrating Voltage
      CalVoltage = CalVoltage / -140.8;  //Y=-140.8x+1408 (if vref is 3.3)
      //-1408 -144.5
      CalTempf = TempAvg - TempCalB;  //Converting analog read into Farenheight (subtracted 1 Feb4th)
      CalTempf = CalTempf / TempCalM;  // y=4.4138x+179.3788
      CalTempc = CalTempf - 32;      //Calculate 
      CalTempc = CalTempc * 5 / 9 ;  //Celcius
  /*    Serial.println(TempCalM);
      Serial.println(TempCalB);
      delay(1000);
  */    
      // advance to the next position in the array:  
      index = index + 1;                    

      // if we're at the end of the array...
      if (index >= numReadings)          {    
        // ...wrap around to the beginning:
        index = 0;  
        //Calculate Level         
      } 
    } 
    if (SkipOne) {  //dont use the first one, it has bad data
        getLevel(false);
      }
      SkipOne = true;   
  }  
}
  
  
    //Deals with incoming Serial Messages
void serialEvent() {
  String IDIn = "";
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
  //  Serial.print("inChar: ");
  //  Serial.println(inChar);  
    InputString += inChar;    // add it to the inputString:
 //   Serial.print("InputString: ");
 //   Serial.println(InputString);  
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the ID
        char IDRead = InputString.charAt(i);
        IDIn += IDRead;
        
      }
      if (IDIn == ID) {   //If the IDs (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        StringComplete = true;    //Tell the main program there is input
        IDIn = "";
      } 
      else if ((InputString.charAt(0) == '0') && (InputString.length() == 2)) {
        //Serial.print("ID is: ");
        Serial.print(ID);  //Print out ID
        Serial.print("/");
        Serial.println(ID);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
      else {
        Serial.print(ID);  //Group ID
        Serial.print("/");
        Serial.println("-1");
      //  Serial.println("ID's dont match");
       // Serial.println(IDIn);
        IDIn = "";
        StringComplete = false;
        InputString = "";
      }
    }  
  }  
  
}


//Processes incoming string
void calcfunction() {
  String ActuatorValString = "";
  int StringLength = InputString.length();
  int x1 = InputString.charAt(9) - '0'; //converting char 9 (command #) to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i < StringLength; i++) {
      char ActuatorRead = InputString.charAt(i);
      ActuatorValString += ActuatorRead;
    }
  }
  
  ActuatorValString.toCharArray(ActuatorValCharArray, ActuatorValString.length());    //convert to Char array for EEPROM
  ActuatorValInt = ActuatorValString.toInt();  //An integer of the serial input
  ActuatorValFloat = ActuatorValString.toFloat();  //A float of the serial input
  //Serial.println(ActuatorValInt);
}



//This Measures the Water level
void getLevel(boolean printout) {
  if (TimeNow > 5000) {
    if (CapVal1 < cap1min) {  //if its less than the minimum, make this the new minimum
      cap1min = CapVal1;     
    }
    if (CapVal2 < cap2min) {
      cap2min = CapVal2;
    }
    if (CapVal3 < cap3min) {
      cap3min = CapVal3;
    }
    if (CapVal4 < cap4min) {
      cap4min = CapVal4;
    }
    if (CapVal5 < cap5min) {
      cap5min = CapVal5;
    }
    if (CapVal6 < cap6min) {
      cap6min = CapVal6;
    }
    if (CapVal7 < cap7min) {
      cap7min = CapVal7;
    }
    if (CapVal8 < cap8min) {
      cap8min = CapVal8;
    }
    if (CapVal9 < cap9min) {
      cap9min = CapVal9;
    }
    if (CapVal10 < cap10min) {
      cap10min = CapVal10;
    }
    if (CapVal11 < cap11min) {
      cap11min = CapVal11;
    }
   
    if (CapVal1 > cap1max) {  //if its more than the max, make this the new max
      cap1max = CapVal1;
    }
    if (CapVal2 > cap2max) {
      cap2max = CapVal2;
    }
    if (CapVal3 > cap3max) {
      cap3max = CapVal3;
    }
    if (CapVal4 > cap4max) {
      cap4max = CapVal4;
    }
    if (CapVal5 > cap5max) {
      cap5max = CapVal5;
    }
    if (CapVal6 > cap6max) {
      cap6max = CapVal6;
    }
    if (CapVal7 > cap7max) {
      cap7max = CapVal7;
    }
    if (CapVal8 > cap8max) {
      cap8max = CapVal8;
    }
    if (CapVal9 > cap9max) {
      cap9max = CapVal9;
    }
    if (CapVal10 > cap10max) {
      cap10max = CapVal10;
    }
    if (CapVal11 > cap11max) {
      cap11max = CapVal11;
    }
  }
  
  boolean Pad1;
  boolean Pad2;
  boolean Pad3;
  boolean Pad4;
  boolean Pad5;
  boolean Pad6;
  boolean Pad7;
  boolean Pad8;
  boolean Pad9;
  boolean Pad10;
  boolean Pad11;
  
  //Check if the pads are wet or dry
  if (CapVal11 > cap11min + ((cap11max - cap11min) / 2))  {
    Pad11 = true;  
  }
    else {
      Pad11 = false;
    }
  if (CapVal10 > cap10min + ((cap10max - cap10min) / 2))  {
    Pad10 = true;
  }
    else {
      Pad10 = false;
    }
  if (CapVal9 > cap9min + ((cap9max - cap9min) / 2)) {
    Pad9 = true;
  }
    else {
      Pad9 = false;
    }
  if (CapVal8 > cap8min + ((cap8max - cap8min) / 2)) {
    Pad8 = true;
  }
    else {
      Pad8 = false;
    }
  if (CapVal7 > cap7min + ((cap7max - cap7min) / 2)) {
    Pad7 = true;
  }
    else {
      Pad7 = false;
    }
  if (CapVal6 > cap6min + ((cap6max - cap6min) / 2)) {  
  Pad6 = true;
  }
    else {
      Pad6 = false;
    }  
  if (CapVal5 > cap5min + ((cap5max - cap5min) / 2)) {  
   Pad5 = true;
  }
    else {
      Pad5 = false;
    } 
  if (CapVal4 > cap4min + ((cap4max - cap4min) / 2)) {
    Pad4 = true;
  }
    else {
      Pad4 = false;
    }
  if (CapVal3 > cap3min + ((cap3max - cap3min) / 2)) { 
    Pad3 = true;
  }
    else {
      Pad3 = false;
    }
  if (CapVal2 > cap2min + ((cap2max - cap2min) / 2)) {
    Pad2 = true;
  }
    else {
      Pad2 = false;
    }
  if (CapVal1 > cap1min + ((cap1max - cap1min) / 2)) {
    Pad1 = true;
  }
    else {
      Pad1 = false;
    }
      
  //Decide What the level is
  if (Pad11) {
    if ((LastWetPad == 11) || (PadChange > 2)){  //make it not flip flop around
      levelval = 110;
      LastWetPad = 11;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad10) {
     if ((LastWetPad == 10) || (PadChange > 2)){
      levelval = 100;
      LastWetPad = 10;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad9) {
     if ((LastWetPad == 9) || (PadChange > 2)){
      levelval = 90;
      LastWetPad = 9;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad8) {
     if ((LastWetPad == 8) || (PadChange > 2)){
      levelval = 80;
      LastWetPad = 8;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }
  else if (Pad7) {
     if ((LastWetPad == 7) || (PadChange > 2)){
      levelval = 70;
      LastWetPad = 7;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad6) {
     if ((LastWetPad == 6) || (PadChange > 2)){
      levelval = 60;
      LastWetPad = 6;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad5) {
     if ((LastWetPad == 5) || (PadChange > 2)){
      levelval = 50;
      LastWetPad = 5;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad4) {
     if ((LastWetPad == 4) || (PadChange > 2)){
      levelval = 40;
      LastWetPad = 4;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad3) {
     if ((LastWetPad == 3) || (PadChange > 2)){
      levelval = 30;
      LastWetPad = 3;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad2) {
     if ((LastWetPad == 2) || (PadChange > 2)){
      levelval = 20;
      LastWetPad = 2;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else if (Pad1) {
     if ((LastWetPad == 1) || (PadChange > 2)){
      levelval = 10;
      LastWetPad = 1;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  } 
  else {
     if ((LastWetPad == 0) || (PadChange > 2)){
      levelval = 0;
      LastWetPad = 0;
      PadChange = 0;
    }
    else {
      PadChange++;
    }
  }

  if(printout) {
    Serial.print(ID);  //Group ID
    Serial.print("/");
    Serial.print("levelval:");
    Serial.println(levelval);
    Serial.print(cap11max);
    Serial.print(",");
    Serial.print(CapVal11);
    Serial.print(",");
    Serial.println(cap11min);
    Serial.print(cap10max);
    Serial.print(",");
    Serial.print(CapVal10);
    Serial.print(",");
    Serial.println(cap10min);
   
    Serial.print(cap9max);
    Serial.print(",");
    Serial.print(CapVal9);
    Serial.print(",");
    Serial.println(cap9min);
    
    Serial.print(cap8max);
    Serial.print(",");
    Serial.print(CapVal8);
    Serial.print(",");
    Serial.println(cap8min);
   
    Serial.print(cap7max);
    Serial.print(",");
    Serial.print(CapVal7);
    Serial.print(",");
    Serial.println(cap7min);
    
    Serial.print(cap6max);
    Serial.print(",");
    Serial.print(CapVal6);
    Serial.print(",");
    Serial.println(cap6min);
    
    Serial.print(cap5max);
    Serial.print(",");
    Serial.print(CapVal5);
    Serial.print(",");
    Serial.println(cap5min);
    
    Serial.print(cap4max);
    Serial.print(",");
    Serial.print(CapVal4);
    Serial.print(",");
    Serial.println(cap4min);
    
    Serial.print(cap3max);
    Serial.print(",");
    Serial.print(CapVal3);
    Serial.print(",");
    Serial.println(cap3min);
    
    Serial.print(cap2max);
    Serial.print(",");
    Serial.print(CapVal2);
    Serial.print(",");
    Serial.println(cap2min);
    
    Serial.print(cap1max);
    Serial.print(",");
    Serial.print(CapVal1);
    Serial.print(",");
    Serial.println(cap1min);
    
    Serial.println(" ");
  }  

  levLED();
} 


//This sets the LED's to show the water level
void levLED(void) {  
  byte off = 0;
  if (levelval == 110) {
    //Turn LED 1-16 on.
    leds.setAllBrightness(LEDHEXbyte);
    leds2.setAllBrightness(LEDHEXbyte);
//    Serial.println("110");
  }
  else if (levelval == 100){
    //Turn LED 1-15 on.  rest off
    leds2.setBrightness(2, LEDHEXbyte);
    leds2.setBrightness(1, LEDHEXbyte);
    leds2.setBrightness(0, LEDHEXbyte);
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("100");
  }
  else if (levelval == 90){
    //Turn LED 1-14 on.  rest off
    leds2.setBrightness(1, LEDHEXbyte);
    leds2.setBrightness(0, LEDHEXbyte);
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("90");
  }
  else if (levelval == 80){
    //Turn LED 1-12 on.  rest off
    leds2.setBrightness(7, LEDHEXbyte);
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("80");
  }
  else if (levelval == 70){
    //Turn LED 1-11 on.  rest off
    leds2.setBrightness(6, LEDHEXbyte);
    leds2.setBrightness(5, LEDHEXbyte);
    leds2.setBrightness(4, LEDHEXbyte); 
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds2.setBrightness(7, off);
    leds.setAllBrightness(LEDHEXbyte); 
//    Serial.println("70");
  }
  else if (levelval == 60){
    //Turn LED 1-9 on.  rest off
    leds2.setBrightness(4, LEDHEXbyte);
    //Turn the rest off
    leds2.setBrightness(3, off);
    leds2.setBrightness(2, off);
    leds2.setBrightness(1, off);
    leds2.setBrightness(0, off);
    leds2.setBrightness(7, off);
    leds2.setBrightness(6, off);
    leds2.setBrightness(5, off);
    leds.setAllBrightness(LEDHEXbyte);
//    Serial.println("60");
  }
  else if (levelval == 50) {
    //Turn LED 1-7 on.  rest off
    leds.setBrightness(5, LEDHEXbyte);
    leds.setBrightness(6, LEDHEXbyte);
    leds.setBrightness(7, LEDHEXbyte);
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds2.setAllBrightness(off);
//    Serial.println("50");
  }
  else if (levelval == 40){  
    //Turn LED 1-6 on.  rest off
    leds.setBrightness(6, LEDHEXbyte);
    leds.setBrightness(7, LEDHEXbyte);
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds2.setAllBrightness(off);
//    Serial.println("40");
  }
  else if (levelval == 30) {
    //Turn LED 1-4 on.
    leds.setBrightness(0, LEDHEXbyte);
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds2.setAllBrightness(off);
//    Serial.println("30");
  }
  else if (levelval == 20){
    //Turn LED 1-3 on
    leds.setBrightness(1, LEDHEXbyte);
    leds.setBrightness(2, LEDHEXbyte);
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds.setBrightness(0, off);
    leds2.setAllBrightness(off);
//    Serial.println("20");
  }
  else if (levelval == 10) {
    //Turn Bottom LED On #1
    leds.setBrightness(3, LEDHEXbyte);
    //Turn the rest off
    leds.setBrightness(4, off);
    leds.setBrightness(5, off);
    leds.setBrightness(6, off);
    leds.setBrightness(7, off);
    leds.setBrightness(0, off);
    leds.setBrightness(1, off);
    leds.setBrightness(2, off);
    leds2.setAllBrightness(off);
//    Serial.println("10");
  }
  else {
    //Turn all LED's OFF
    leds2.setAllBrightness(off);
    leds.setAllBrightness(off);
//    Serial.println("0");
  } 
}

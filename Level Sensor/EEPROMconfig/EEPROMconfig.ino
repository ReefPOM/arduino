
/*This program loads the Level Sensor with necessary EEPROM data.
 *Run this program first, then the real program named for the PCB
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefPOM.com
 *
 *
Arduino Pinout: 
2 = capsense fill 1
3 = capsense read 1
4 = capsense read 2
5 = capsense read 6
6 = capsense read 7
7 = capsense read 8
8 = capsense read 9
9 = capsense read 10
10 = capsense read 5
11 = capsense read 4
12 = capsense read 11
13 = capsense read 3
14 = Flow senser power
15 = Ambient Light Sensor
16 = Temp read
17 = Flow read
21 = 3.3v reference

Level Sensor Element List
Element, Function, Pin#
1  ,  Flow    ,  17  (pin 14 is flow power)
2  ,  TempF   ,  16
3  ,  TempC   ,  16
4  ,  Level   ,
5  ,  AmbLight,  15
6  ,  Voltage ,  21
7  ,  LEDB    ,     LED brightness, amb controlled/not, dimming Factor
*/

#include <EEPROMex.h>  //For Calibration Data and ID

void setup() {
    // put your setup code here, to run once:
    //Load stock Sensor Element ID's
  EEPROM.updateBlock<char>(0,"gcp00000", 8);   //ID
      
  EEPROM.update(8, 1);  //Enable Watchdog Timer
  EEPROM.updateBlock<char>(9,"atr00001", 8);   //Element #1 ID
  EEPROM.updateBlock<char>(28,"atr00002", 8);  //Element #2 ID
  EEPROM.updateBlock<char>(47,"atr00003", 8);  //Element #3 ID
  EEPROM.updateBlock<char>(66,"atr00004", 8);  //Element #4 ID
  EEPROM.updateBlock<char>(85,"azt00001", 8);  //Element #5 ID
  EEPROM.updateBlock<char>(104,"azt00002", 8);  //Element #6 ID
  EEPROM.updateBlock<char>(123,"azt00003", 8);  //Element #7 ID
  EEPROM.updateBlock<char>(142,"zzzzzzzz", 8);  //Element #8 ID
  EEPROM.updateBlock<char>(161,"zzzzzzzz", 8);  //Element #9 ID
  EEPROM.updateBlock<char>(180,"zzzzzzzz", 8);  //Element #10 ID
  EEPROM.updateBlock<char>(199,"zzzzzzzz", 8);  //Element #11 ID
  EEPROM.updateBlock<char>(218,"zzzzzzzz", 8);  //Element #12 ID
  EEPROM.updateBlock<char>(237,"zzzzzzzz", 8);  //Element #13 ID
  EEPROM.updateBlock<char>(256,"zzzzzzzz", 8);  //Element #14 ID
  EEPROM.updateBlock<char>(275,"zzzzzzzz", 8);  //Element #15 ID  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(294,"zzzzzzzz", 8);  //Element #16 ID
  EEPROM.updateBlock<char>(313,"zzzzzzzz", 8);  //Element #17 ID
  EEPROM.updateBlock<char>(332,"zzzzzzzz", 8);  //Element #18 ID
  EEPROM.updateBlock<char>(351,"zzzzzzzz", 8);  //Element #19 ID
  EEPROM.updateBlock<char>(370,"zzzzzzzz", 8);  //Element #20 ID

    //write stock Element Types
        //s=sensor, a=actuator, n=nothing, z=Loaded by this program
  EEPROM.updateBlock<char>(17,"s", 1);  //Element #1 Type
  EEPROM.updateBlock<char>(36,"s", 1);  //Element #2 Type
  EEPROM.updateBlock<char>(55,"s", 1);  //Element #3 Type
  EEPROM.updateBlock<char>(74,"s", 1);  //Element #4 Type
  EEPROM.updateBlock<char>(93,"s", 1);  //Element #5 Type
  EEPROM.updateBlock<char>(112,"s", 1);  //Element #6 Type
  EEPROM.updateBlock<char>(131,"a", 1);  //Element #7 Type
  EEPROM.updateBlock<char>(150,"z", 1);  //Element #8 Type
  EEPROM.updateBlock<char>(169,"z", 1);  //Element #9 Type
  EEPROM.updateBlock<char>(188,"z", 1);  //Element #10 Type
  EEPROM.updateBlock<char>(207,"z", 1);  //Element #11 Type
  EEPROM.updateBlock<char>(226,"z", 1);  //Element #12 Type
  EEPROM.updateBlock<char>(245,"z", 1);  //Element #13 Type
  EEPROM.updateBlock<char>(264,"z", 1);  //Element #14 Type
  EEPROM.updateBlock<char>(283,"z", 1);  //Element #15 Type  //Fill the unused ones with z's
  EEPROM.updateBlock<char>(302,"z", 1);  //Element #16 Type
  EEPROM.updateBlock<char>(321,"z", 1);  //Element #17 Type
  EEPROM.updateBlock<char>(340,"z", 1);  //Element #18 Type
  EEPROM.updateBlock<char>(359,"z", 1);  //Element #19 Type
  EEPROM.updateBlock<char>(378,"z", 1);  //Element #20 Type

    //Write stock Element Functions
       //0=unused, 1=analogRead, 2=digitalRead,
       //3=analogWrite, 4=digitalWrite, 5=triac, 6=flow, 7=level, 10=Empty
  EEPROM.updateInt(18, 6);  //Element #1 Function
  EEPROM.updateInt(37, 1);  //Element #2 Function
  EEPROM.updateInt(56, 1);  //Element #3 Function
  EEPROM.updateInt(75, 7);  //Element #4 Function
  EEPROM.updateInt(94, 1);  //Element #5 Function
  EEPROM.updateInt(113, 1);  //Element #6 Function
  EEPROM.updateInt(132, 3);  //Element #7 Function 
  EEPROM.updateInt(151, 10);  //Element #8 Function  //FIll the unused ones with 10's
  EEPROM.updateInt(170, 10);  //Element #9 Function
  EEPROM.updateInt(189, 10);  //Element #10 Function
  EEPROM.updateInt(208, 10);  //Element #11 Function
  EEPROM.updateInt(227, 10);  //Element #12 Function
  EEPROM.updateInt(246, 10);  //Element #13 Function
  EEPROM.updateInt(265, 10);  //Element #14 Function
  EEPROM.updateInt(284, 10);  //Element #15 Function
  EEPROM.updateInt(303, 10);  //Element #16 Function
  EEPROM.updateInt(322, 10);  //Element #17 Function
  EEPROM.updateInt(341, 10);  //Element #18 Function
  EEPROM.updateInt(360, 10);  //Element #19 Function
  EEPROM.updateInt(379, 10);  //Element #20 Function
  
    //Write stock Calibration data if the element is a sensor 
        //or Fail Safe Values / Extras they are actuators
  EEPROM.updateFloat(20, 1);  //Element #1 Slope / FS Val
  EEPROM.updateFloat(24, 0);  //Element #1 Y-Intercept / Extra
  EEPROM.updateFloat(39, 1);  //Element #2 Slope / FS Val
  EEPROM.updateFloat(43, 0);  //Element #2 Y-Intercept / Extra
  EEPROM.updateFloat(58, 1);  //Element #3 Slope / FS Val
  EEPROM.updateFloat(62, 0);  //Element #3 Y-Intercept / Extra
  EEPROM.updateFloat(77, 1);  //Element #4 Slope / FS Val
  EEPROM.updateFloat(81, 0);  //Element #4 Y-Intercept / Extra
  EEPROM.updateFloat(96, 1);  //Element #5 Slope / FS Val
  EEPROM.updateFloat(100, 0);  //Element #5 Y-Intercept / Extra
  EEPROM.updateFloat(115, 1);  //Element #6 Slope / FS Val
  EEPROM.updateFloat(119, 0);  //Element #6 Y-Intercept / Extra
  EEPROM.updateFloat(134, 1);  //Element #7 Slope / FS Val
  EEPROM.updateFloat(138, 0);  //Element #7 Y-Intercept / Extra
  EEPROM.updateFloat(153, 1);  //Element #8 Slope / FS Val
  EEPROM.updateFloat(157, 0);  //Element #8 Y-Intercept / Extra
  EEPROM.updateFloat(172, 1);  //Element #9 Slope / FS Val
  EEPROM.updateFloat(176, 0);  //Element #9 Y-Intercept / Extra
  EEPROM.updateFloat(191, 1);  //Element #10 Slope / FS Val
  EEPROM.updateFloat(195, 0);  //Element #10 Y-Intercept / Extra
  EEPROM.updateFloat(210, 1);  //Element #11 Slope / FS Val
  EEPROM.updateFloat(214, 0);  //Element #11 Y-Intercept / Extra
  EEPROM.updateFloat(229, 1);  //Element #12 Slope / FS Val
  EEPROM.updateFloat(233, 0);  //Element #12 Y-Intercept / Extra
  EEPROM.updateFloat(248, 1);  //Element #13 Slope / FS Val
  EEPROM.updateFloat(252, 0);  //Element #13 Y-Intercept / Extra
  EEPROM.updateFloat(267, 1);  //Element #14 Slope / FS Val
  EEPROM.updateFloat(271, 0);  //Element #14 Y-Intercept / Extra
  EEPROM.updateFloat(286, 1);  //Element #15 Slope / FS Val
  EEPROM.updateFloat(290, 0);  //Element #15 Y-Intercept / Extra
  EEPROM.updateFloat(305, 1);  //Element #16 Slope / FS Val
  EEPROM.updateFloat(309, 0);  //Element #16 Y-Intercept / Extra
  EEPROM.updateFloat(324, 1);  //Element #17 Slope / FS Val
  EEPROM.updateFloat(328, 0);  //Element #17 Y-Intercept / Extra
  EEPROM.updateFloat(343, 1);  //Element #18 Slope / FS Val
  EEPROM.updateFloat(347, 0);  //Element #18 Y-Intercept / Extra
  EEPROM.updateFloat(362, 1);  //Element #19 Slope / FS Val
  EEPROM.updateFloat(366, 0);  //Element #19 Y-Intercept / Extra
  EEPROM.updateFloat(381, 1);  //Element #20 Slope / FS Val
  EEPROM.updateFloat(385, 0);  //Element #20 Y-Intercept / Extra
  
  //Element Pins
  EEPROM.updateInt(389, 17);  //Element #1 Pin
  EEPROM.updateInt(391, 16);  //Element #2 Pin
  EEPROM.updateInt(393, 16);  //Element #3 Pin
  EEPROM.updateInt(395, 99);  //Element #4 Pin
  EEPROM.updateInt(397, 15);  //Element #5 Pin
  EEPROM.updateInt(399, 21);  //Element #6 Pin
  EEPROM.updateInt(401, 99);  //Element #7 Pin    //Fill the unused ones with 99's
  EEPROM.updateInt(403, 99);  //Element #8 Pin
  EEPROM.updateInt(405, 99);  //Element #9 Pin
  EEPROM.updateInt(407, 99);  //Element #10 Pin
  EEPROM.updateInt(409, 99);  //Element #11 Pin
  EEPROM.updateInt(411, 99);  //Element #12 Pin
  EEPROM.updateInt(413, 99);  //Element #13 Pin
  EEPROM.updateInt(415, 99);  //Element #14 Pin
  EEPROM.updateInt(417, 99);  //Element #15 Pin
  EEPROM.updateInt(419, 99);  //Element #16 Pin
  EEPROM.updateInt(421, 99);  //Element #17 Pin
  EEPROM.updateInt(423, 99);  //Element #18 Pin
  EEPROM.updateInt(425, 99);  //Element #19 Pin
  EEPROM.updateInt(427, 99);  //Element #20 Pin

  //Level Sensor Max and Min Cap Vals
  EEPROM.updateInt(429, 0);  //Cap 11 Max
  EEPROM.updateInt(431, 0);  //Cap 11 Min
  EEPROM.updateInt(433, 0);  //Cap 10 Max
  EEPROM.updateInt(435, 0);  //Cap 10 Min
  EEPROM.updateInt(437, 0);  //Cap 9 Max
  EEPROM.updateInt(439, 0);  //Cap 9 Min
  EEPROM.updateInt(441, 0);  //Cap 8 Max
  EEPROM.updateInt(443, 0);  //Cap 8 Min
  EEPROM.updateInt(445, 0);  //Cap 7 Max
  EEPROM.updateInt(447, 0);  //Cap 7 Min
  EEPROM.updateInt(449, 0);  //Cap 6 Max
  EEPROM.updateInt(451, 0);  //Cap 6 Min
  EEPROM.updateInt(453, 0);  //Cap 5 Max
  EEPROM.updateInt(455, 0);  //Cap 5 Min
  EEPROM.updateInt(457, 0);  //Cap 4 Max
  EEPROM.updateInt(459, 0);  //Cap 4 Min
  EEPROM.updateInt(461, 0);  //Cap 3 Max
  EEPROM.updateInt(463, 0);  //Cap 3 Min
  EEPROM.updateInt(465, 0);  //Cap 2 Max
  EEPROM.updateInt(467, 0);  //Cap 2 Min
  EEPROM.updateInt(469, 0);  //Cap 1 Max
  EEPROM.updateInt(471, 0);  //Cap 1 Min
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
